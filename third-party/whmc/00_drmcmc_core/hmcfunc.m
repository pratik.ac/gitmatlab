function [wnew,accept,err,P] = hmcfunc(wcurr,llhfunc,gradfunc,hmc)

dim = length(wcurr);
iM = hmc.iM;
sM = hmc.sM;
lfnum = hmc.lfnum;
lfsize = hmc.lfsize;

wt = wcurr;
p = sM*randn(dim,1);    
Ex = -llhfunc(wt');
Hx = Ex + 0.5*(p'*iM*p);

% randomize leapfrog jumps
if 1
    lfnum_t = randi([lfnum-1 lfnum+1]);
    lfsize_t = lfsize+ (rand()-0.5)*lfsize/10;
end

% leapfrogs
for i=1:lfnum_t

    dW = gradfunc(wt)';
    p = p + 0.5*lfsize_t*dW;

    wt = wt + lfsize_t*iM*p;

    dW = gradfunc(wt)';      
    p = p + 0.5*lfsize_t*dW;

end

Ey = -llhfunc(wt');
Hy = Ey + 0.5*(p'*iM*p);

% accept-reject
P = min(1,exp(Hx-Hy));
accept = rand < P;

%% Accept-Reject 
if accept == 1
    wnew = wt;
    err = Ey;
else
    wnew = wcurr;
    err = Ex;   
end