function [llh,grad] = funcslogmog(mog)

    llh = @(w)llhmog(w,mog);
    grad = @(w)gradlogmog(w,mog);


    %% log likelihood
    function llh = llhmog(X,mog)

        %x = x';
        mu = mog.mu;
        cv = mog.cv;
        pik = mog.pik;

        [~, k] = size(mu);
        n = size(X,1);

        logpk = zeros(n,k);
        for i=1:k
            logpk(:,i) = logmvnpdf(X,mu(:,i)',cv(:,:,i));
        end

        logpk = logpk + repmat(log(pik),n,1);
        llh = logsumexp(logpk,2);

    end

    %% grad log mog
    function dlogmog = gradlogmog(w,mog)
        
        mu = mog.mu; cv = mog.cv; pik = mog.pik;
        
        [dim,k] = size(mu);
        b = zeros(k,dim);
        logpkx = zeros(1,k);

        for kk=1:k
            mui = mu(:,kk);
            cvi = cv(:,:,kk);
            logpkx(kk) = logmvnpdf(w,mui,cvi);
            b(kk,:) = cvi\(mui-w);           
        end

        ret = 0;
        for kk=1:k
            ret = ret + pik(kk)*b(kk,:)/(exp(logpkx - repmat(logpkx(kk),1,k))*pik');
        end

        dlogmog = ret;

        if isnan(dlogmog)
            error('nan');
        end
        
    end
      

end