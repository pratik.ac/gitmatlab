This package of codes is to replicate the results in the following paper:

Wormhole Hamiltonian Monte Carlo
Shiwei Lan, Jeffrey Streets, Babak Shahbaba

http://arxiv.org/abs/1306.0063


Some codes are modified from Sungjin Anh's following paper:

Distributed and Adaptive Darting Monte Carlo through Regenerations
Sungjin Ahn, Yutian Chen, Max Welling

http://jmlr.org/proceedings/papers/v31/ahn13a.html

We give thanks to Sungjin Anh for sharing his code of Darting Regenerative Monte Carlo (DRMC).

Please give appropriate references when using these codes, thanks!

08-01-2013