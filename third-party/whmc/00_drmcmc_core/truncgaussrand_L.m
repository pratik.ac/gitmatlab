function prop = truncgaussrand_L(mu,L,tr_rad,dim,uniform)
% sampling from a stardard Gaussian truncated by a radius, tr_rad.

if ~exist('uniform', 'var') || isempty(uniform)
  uniform = false; % Do uniform sampling for darting
end

if ~uniform % Drawn from a Truncated Gaussian

  mu_r = sqrt(dim-1)+0.001;     % compute mean of the Gamma
  alpha = 0.5*dim;
  beta = 1;

  %% radius follows a Gamma distribution on unitv not r
  r2u = @(rr) 0.5*rr^2;
  u2r = @(uu) sqrt(2*uu);

  %% sample unitv vector
  % unitv = mvnrnd(zeros(dim,1),eye(dim));
  unitv = randn(1, dim);
  unitv = unitv/norm(unitv);

  %% sample r
  if tr_rad > mu_r 

      % sample r from gamrnd()
      r = Inf;
      numreject = -1;
      while r > tr_rad
          numreject = numreject + 1;
          u = gamrnd(alpha,beta);
          r = u2r(u);
      end

  else
      % sample r by rejection sampling   
%       unifenvel = gampdf(r2u(tr_rad),alpha,beta);
%       accept = 0;
%       numreject = 0;
%       while ~accept
% 
%           r = rand*tr_rad;
%           r_pdf = gampdf(r2u(r),alpha,beta);
% 
%           accept = rand < min(1,exp(log(r_pdf) - log(unifenvel)));
% 
%           if ~accept
%               numreject = numreject + 1;
%           end
% 
%       end
      
      u = gaminv(rand * gamcdf(tr_rad, alpha, beta),alpha,beta);
      r = u2r(u);
      
  end

  prop = mu + L*r*unitv';
  
else % Drawn from a uniform distribution
  
  %% sample unitv vector
  % unitv = mvnrnd(zeros(dim,1),eye(dim));
  unitv = randn(1, dim);
  unitv = unitv/norm(unitv);
  
  %% sample r
  u = rand;
  r = u^(1 / dim) * tr_rad;
  
  prop = mu + L*r*unitv';
  
end
  
  
  