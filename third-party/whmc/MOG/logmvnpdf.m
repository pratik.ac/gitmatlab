function logpdf = logmvnpdf(x,mu,cv,L)
% x: N x D
% mu: 1 x D
% cv/L: D x D

if nargin > 3
  d = size(x, 2);
  x = bsxfun(@minus, x, mu);
  xLinv = x / L'; % N x D
  logSqrtDetSigma = sum(log(diag(L)));
  quadform = sum(xLinv.^2, 2);
  
  logpdf = -0.5*quadform - logSqrtDetSigma - d*log(2*pi)/2;
else
  [~,logpdf] = mvnpdf(x,mu,cv);
end
