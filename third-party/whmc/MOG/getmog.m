function [mu cv invcv] = getmog(k,seed,dim,dist)

stream = RandStream('mt19937ar','Seed',seed);
% RandStream.setDefaultStream(stream);
RandStream.setGlobalStream(stream);

% mu = zeros(dim,k);
% if k == 1
%         
%     % zero mean
%     
% elseif k == 2
%         
%     mu(:,1) = ones(dim,1)*dist + randn(dim,1);
%     mu(:,2) = ones(dim,1)*(-dist)+ randn(dim,1);
%     
% elseif k == 3
%         
%     mu(:,1) = ones(dim,1)*dist + randn(dim,1);
%     mu(:,2) = ones(dim,1)*(-dist)+ randn(dim,1);
%     mu(:,3) = ones(dim,1)*2*dist.*sign(randn(dim,1)) + randn(dim,1);
%     mu(1:ceil(dim/2),3) = mu(1:ceil(dim/2),3)*(-1);  
%     % mu(:,3) = ones(dim,1)*randi(dist).*sign(randn(dim,1)) + randn(dim,1);
%     % mu(:,3) = ones(dim,1)*dist + randn(dim,1);
%     % mu(1:ceil(dim/2),3) = mu(1:ceil(dim/2),3)*(-1);  
%     
% elseif k > 3
%         
%      mu = (rand(dim,k)-0.5)*dim*dist;    
%      % centralize mu     
%     
% end

% mu = (rand(dim,k)-0.5)*sqrt(dim/2)*dist;     
expand = dist*k^(1/dim);
mu = rand(dim,k)*expand;    % % expand^dim = k*dist^dim;         
% centralize
mu = mu - repmat(mean(mu,2),1,k);

cv = zeros(dim,dim,k);
invcv = zeros(size(cv));

for i = 1:k    
        
        S = randn(dim);%/dim;
        c = S*S' + 2*eye(dim);
        %     c = eye(dim);
        cv(:,:,i) = c*(1/dim);
        invcv(:,:,i) = inv(c);
        [~,eval] = eig(c);
        
        if any(sign(diag(eval)) < 0)
                error('cov is not a positive definite or symmetric');
        end
        
end