function[] = RRT()
clear all;clc; clf;
tree = [];
buildRRT();
%plot(tree.x,tree.y,'ro-');
%plot_tree(tree)

function [] = plotTree(tree)
for i=1:length(tree),
    n = tree(1);
    np = n.parent;
    if np ~= inf
        plot([np.x, np.y], [n.x, n.y], 'r.-');
    end
end

function[] = buildRRT()
global tree;
x0=0; y0=0; xGoal=5; yGoal=5;
node.x=x0; node.y=y0; node.parent=0;
epsilon=1;
tree = [tree, node];

while(norm([node.x-xGoal,node.y-yGoal]) > epsilon)
    nodeidx = randi([1, length(tree)]);
    node = generateSample( tree(nodeidx));
    node.parent=inf;
    node = nearestVertex(node);
    % plot(node.x,node.y,'ro-'); hold on; axis([-10 10 -10 10]); pause(0.1);
    fprintf('id: %d, (%.2f, %.2f) p: %d\n', length(tree), node.x, node.y, node.parent);
    plot([node.x, tree(node.parent).x], [node.y, tree(node.parent).y], 'r.-');
    hold on; axis([-10 10 -10 10]);
    pause(0.1);
end


function[node] = nearestVertex(node)
global tree;
n=length(tree); d = zeros(n,1);
dmin=1000; index=0;
for i=1:n
    d(i) = norm([tree(i).x-node.x, tree(i).y-node.y]);
    if d(i)<dmin
        dmin=d(i);
        index=i;
    end
end
node.parent=index;
tree = [tree, node];

function[s_new] = generateSample(s0)
% for a system given by dynamics
% \dot x = u, \dot y = v, |u|,|v| <1

dt=5; % time step

u = -1 + 2*rand(1); % uniform random number between -1 to 1
v = -1 + 2*rand(1);
s_new.x = s0.x + u*dt; % new sample
s_new.y = s0.y + v*dt;

if collisionCheck(s_new)
    s_new = generateSample(s0);
end

function [inCollision] = collisionCheck(s_new)
r_obst = 0.2;
x_obst = 2.5;
y_obst = 2.5;

if norm([x_obst-s_new.x,y_obst-s_new.y])<r_obst
    inCollision = 1;
else
    inCollision = 0;
end
