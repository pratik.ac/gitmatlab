function [SAMPBURNIN SAMP INTG s] = init_wsn(dim,iterburnin,nrestart,...
        ninitsamp,mxit,hmc,itvplot)

burninPerRestart = ceil(iterburnin / nrestart);
iterburnin = burninPerRestart * nrestart;
initPerRestart =ceil(ninitsamp / nrestart);
ninitsamp = initPerRestart * nrestart;
mxit = max(mxit, ninitsamp);

SAMPBURNIN = zeros(dim, iterburnin);
SAMP = zeros(dim,mxit*2);
INTG = zeros(1,mxit*2);
s_burnin = 0;
s = 0;
std_init = 2;
idx_restart = 0;

while idx_restart < nrestart
        
        idx_restart = idx_restart + 1;

        % Restart
        w = (rand(dim, 1)-.5)*std_init+.5;
        avgprhmcaccept = 0;
        
        for t = 1 : burninPerRestart + initPerRestart
                
                %     [w,~,~,prhmcaccept] = hmcfunc(w,llhfunc,gradfunc,hmc);
                %     [w, ~, prhmcaccept] = hmc_step(w, llhfunc, gradfunc, hmc);
                [w, ~, prhmcaccept] = hmc_step_wsn(w, hmc);
                prhmcaccept = min(1, prhmcaccept);
                avgprhmcaccept = (1-1/t) * avgprhmcaccept + 1/t * prhmcaccept;

                if t <= burninPerRestart
                        
                        s_burnin = s_burnin + 1;
                        SAMPBURNIN(:,s_burnin) = w;
                
                else
                        
                        if t == burninPerRestart + 1 && avgprhmcaccept < .3
                                s_burnin = s_burnin - burninPerRestart;
                                idx_restart = idx_restart - 1;
                                break;
                        end

                        s = s + 1;
                        SAMP(:,s) = w;
                        
                end
                
                last_good_w = w;
                
                if ~rem(t+itvplot-1,itvplot)
                        fprintf('Burn-in iter: %d, accept rate: %f\n', t, avgprhmcaccept);
                end
                
        end
end
       




