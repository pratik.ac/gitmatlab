function [ta, accept, Q] = hmc_step_wsn(ta, params)
% HMC params:
%   eta: step size
%   M: #leapfrog steps
%   L: pre-conditioning matrix (chol), C = L * L'
eta = params.eta * ((2 * rand - 1) * .01 + 1);
M = max(1, params.M + max(2, params.M / 10) * (2 * rand - 1));
L = params.L;
D = length(ta);

Xb = params.Xb;
Yb = params.Yb;
Ys = params.Ys;
N = params.N;
R = params.R;
sig = params.sig;


ta0 = ta;

% Draw p
noise0 = randn(D, 1);
p = L' \ noise0;

% Half step
p = p + eta / 2 * wsn_grad(ta, Xb, Yb, Ys, N, R, sig);

% M - 1 steps
for m = 1 : M - 1
  ta = ta + eta * L * (L' * p);
  
  p = p + eta * wsn_grad(ta, Xb, Yb, Ys, N, R, sig);
end

% Last step
ta = ta + eta * L * (L' * p);

p = p + eta / 2 * wsn_grad(ta, Xb, Yb, Ys, N, R, sig);

noise = L' * p;


Q = exp((wsn_llh(ta, Xb, Yb, Ys, N, R, sig) - 1/2 * (noise' * noise)) -...
        (wsn_llh(ta0, Xb, Yb, Ys, N, R, sig) - 1/2 * (noise0' * noise0)));
if isnan(Q)
  Q = 0;
end
accept = rand < Q;

if ~accept
  ta = ta0;
end

end

function f = wsn_llh(ta, Xb, Yb, Ys, N, R, sig)
X = reshape(ta, N, 2);

Obs = [Ys ~= 0, Yb ~= 0];

dx = bsxfun(@minus, X(:,1), [X(:,1)' Xb(:,1)']);
dy = bsxfun(@minus, X(:,2), [X(:,2)' Xb(:,2)']);
dist2 = dx.^2 + dy.^2 + 1e-10;

logPo = -1/(2 * R^2) * dist2  - 1e-10;

ll1 = log(1 - exp(logPo));
ll2 = logPo - 1/2*log(2*pi*sig^2) - 1/(2*sig^2) * (sqrt(dist2) - [Ys, Yb]).^2;

f = sum(sum(triu((1 - Obs) .* ll1 + Obs .* ll2, 1)));
end

function g = wsn_grad(ta, Xb, Yb, Ys, N, R, sig)
X = reshape(ta, N, 2);

Obs = [Ys ~= 0, Yb ~= 0];

dx = bsxfun(@minus, X(:,1), [X(:,1)' Xb(:,1)']);
dy = bsxfun(@minus, X(:,2), [X(:,2)' Xb(:,2)']);
dist2 = dx.^2 + dy.^2 + 1e-10;

logPo = -1/(2 * R^2) * dist2;
Po = exp(logPo);

g1 = Po./(1-Po) / R^2;
g2 = -1 / R^2 - (1 - [Ys, Yb]./sqrt(dist2)) / sig^2;

g1x = g1 .* dx;
g1y = g1 .* dy;
g2x = g2 .* dx;
g2y = g2 .* dy;

gx = sum((1 - Obs) .* g1x + Obs .* g2x, 2) - diag(g1x);
gy = sum((1 - Obs) .* g1y + Obs .* g2y, 2) - diag(g1y);
g = [gx; gy];

end

