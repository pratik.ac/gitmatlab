function [wnew, accept] = sample_RWM_wsn(w, noise, llhfunc)

dim = length(w);

wprop = w + sqrt(noise)*randn(dim,1);
ratio = min(1, exp(llhfunc(wprop) - llhfunc(w)));
accept = rand < ratio;               

if accept
        wnew = wprop;
else
        wnew = w;
end
