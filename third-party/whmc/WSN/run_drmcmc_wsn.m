function run_drmcmc_wsn(seed,dim,la,troff,trquant,REJECPROP,m,maxncompo,...
    ninitsamp,iterburnin,nrestart,std_init,time4adapt,nsubsamp,mxit,lfnum,eta,itvplot,itvsave,PLOT)

% clear
% close all
dbstop if error
format compact
% addpath ../vdpgm
addpath('wsn_data');
addpath('from_sungjin');

global t

if isdeployed
  seed = str2double(seed);
  dim = str2double(dim);
  la = str2double(la);
  troff = str2double(troff);
  trquant = str2double(trquant);
  REJECPROP = str2double(REJECPROP);
  m = str2double(m);
  maxncompo = str2double(maxncompo);
  ninitsamp = str2double(ninitsamp);
  iterburnin = str2double(iterburnin);
  time4adapt = str2double(time4adapt);
  nsubsamp = str2double(nsubsamp);
  mxit = str2double(mxit);
  eta = str2double(eta);
  itvplot = str2double(itvplot);
  itvsave = str2double(itvsave);
  PLOT = str2double(PLOT);
end

hmc_params = struct('M', lfnum, 'eta', eta, 'L', 1);

%%
figrow = 2;
dpgmalgo = 1;

%% load data: X
% load data/MEG_art MEG_art
% Xdim = sqrt(dim);
% assert(Xdim == ceil(Xdim) && Xdim <= size(MEG_art, 1));
% X = MEG_art(1 : Xdim, :);
% clear MEG_art

if dim == 6
  ld = load('wsn_data/data_wsn_N6.mat', 'Xb', 'Yb', 'Ys', 'N', 'R', 'sig');
elseif dim == 8
  ld = load('wsn_data/data_wsn_N8.mat', 'Xb', 'Yb', 'Ys', 'N', 'R', 'sig');
end
hmc_params.Xb = ld.Xb;
hmc_params.Yb = ld.Yb;
hmc_params.Ys = ld.Ys;
hmc_params.N = ld.N;
hmc_params.R = ld.R;
hmc_params.sig = ld.sig;

% Problem dimensionality
dim = ld.N * 2;
tgradius = sqrt(dim-1) + troff;

%% get function handles for lr
[llhfunc, gradfunc] = funs_wsn(ld.Xb, ld.Yb, ld.Ys, ld.N, ld.R, ld.sig);
clear ld

%% fname
fname = sprintf('wsn_d%d_troff%2.1f_reject%d_t4adpt%d_mxk%d_nssub%d_nsinit%d_m%1.1f_sd%d',...
    dim,troff,REJECPROP,time4adapt,maxncompo,nsubsamp,ninitsamp,m,seed);
fname = strrep(fname,'.','-');
fname = appendfilehead(fname);
fname = strcat('_',fname);

%% random seed
RandStream.setDefaultStream(RandStream('mt19937ar','Seed',seed));

%% plots
% fig.contour = figure(428); clf;
fig.hist3 = figure(448); clf;
fig.regenrate = figure(450); clf;
fig.logC = figure(451); clf;
fig.regenratevsit = figure(452); clf;

if PLOT
    set(fig.hist3,'windowstyle','docked');
    set(fig.regenrate,'windowstyle','docked');
    set(fig.regenratevsit,'windowstyle','docked');
    set(fig.logC,'windowstyle','docked');
end

pause(0.5);

numfig = figrow^2;
cvpair = [1:numfig; mod((1:numfig) + dim/2 -1, dim)+1]';
% %
% % %% draw components
% % color = {'rx','bx','kx'};
set(0,'CurrentFigure',fig.hist3);
for f=1:numfig
    subplot(figrow,figrow,f);
% %     hold on;
% %     ii = cvpair(f,1);
% %     jj = cvpair(f,2);
% %     %plot(mn_h(ii),mn_h(jj),'ro','linewidth',1,'markersize',5);
% %     plotGauss(wmode(ii),wmode(jj),wcov(ii,ii),wcov(jj,jj),wcov(ii,jj),2,'--k',1);
% %     plot(wmode(ii),wmode(jj),'b+','linewidth',1,'markersize',5);
    axax(f,:) = axis;
end
% % drawnow;

% %% draw mog contour
% ax = max(abs(axax),[],1);
% ax([1 3]) = -1*ax([1 3]);
% rangx = linspace(ax(1),ax(2),100);
% rangy = linspace(ax(3),ax(4),100);
% [xx yy] = meshgrid(rangx,rangy);
% xv = xx(:);
% yv = yy(:);
% xy = [xv yv];
% prxy = zeros([size(xx)  mog.k]);
% for i=1:numfig
%     subplot(figrow,figrow,i); hold on;
%     ii = cvpair(i,1);
%     jj = cvpair(i,2);
%     density = exp(logmogpdf(xy,mog.mu([ii jj],:),mog.cv([ii,jj],[ii,jj],:),mog.pik));
%     prxy(:,:,i) = reshape(density,length(rangy),length(rangx));
%     contour(xx, yy,prxy(:,:,i),15);
% end
% drawnow;


%% inits
burninPerRestart = ceil(iterburnin / nrestart);
iterburnin = burninPerRestart * nrestart;
initPerRestart =ceil(ninitsamp / nrestart);
ninitsamp = initPerRestart * nrestart;
mxit = max(mxit, ninitsamp);

SAMPBURNIN = zeros(dim, iterburnin);
SAMP = zeros(dim,mxit*2);
INTG = zeros(1,mxit*2);
s_burnin = 0;
s = 0;
avgmaxfreqwx = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% burnin + preliminary run to build initial dpmm
if 1
  idx_restart = 0;
  while idx_restart < nrestart
    idx_restart = idx_restart + 1;
    
    % Restart
    w = (rand(dim, 1)-.5)*std_init+.5;
    avgprhmcaccept = 0;
    for t = 1 : burninPerRestart + initPerRestart
      %     [w,~,~,prhmcaccept] = hmcfunc(w,llhfunc,gradfunc,hmc);
      %     [w, ~, prhmcaccept] = hmc_step(w, llhfunc, gradfunc, hmc_params);
      [w, ~, prhmcaccept] = hmc_step_wsn(w, hmc_params);
      prhmcaccept = min(1, prhmcaccept);
      avgprhmcaccept = (1-1/t) * avgprhmcaccept + 1/t * prhmcaccept;
      
      if t <= burninPerRestart
        s_burnin = s_burnin + 1;
        SAMPBURNIN(:,s_burnin) = w;
      else
        if t == burninPerRestart + 1 && avgprhmcaccept < .3
          s_burnin = s_burnin - burninPerRestart;
          idx_restart = idx_restart - 1;
          break;
        end
        
        s = s + 1;
        SAMP(:,s) = w;
      end
      
      %% plot
      %     itvplot = 50;
      if ~rem(t+itvplot-1,itvplot)
        fprintf('Burn-in iter: %d, accept rate: %f\n', t, avgprhmcaccept);
        set(0,'CurrentFigure',fig.hist3); clf;
        for ff=1:numfig
          subplot(figrow,figrow,ff);
          ii = cvpair(ff,1);
          jj = cvpair(ff,2);
          % contour(xx, yy,prxy(:,:,ff),15); hold on;
          if t <= burninPerRestart
            plot(SAMPBURNIN(ii,1:10:s_burnin),SAMPBURNIN(jj,1:10:s_burnin),'b.','markersize',1); hold on;
            plot(w(ii),w(jj),'bo','markersize',2,'linewidth',2); hold off
          else
            plot(SAMP(ii,1:10:s),SAMP(jj,1:10:s),'b.','markersize',1); hold on
            plot(w(ii),w(jj),'bo','markersize',2,'linewidth',2); hold off
          end
          %                 plot(mn_h(ii),mn_h(jj),'ro','linewidth',1,'markersize',5);
          %                 plotGauss(mn_h(ii),mn_h(jj),cv_h(ii,ii),cv_h(jj,jj),cv_h(ii,jj),2,'--k',1);
          % %             plotGauss(wmode(ii),wmode(jj),wcov(ii,ii),wcov(jj,jj),wcov(ii,jj),2,'-b',1);
          %             axis(axax(ff,:));
        end
        drawnow
      end
      
      last_good_w = w;
    end
  end
  
else
  
  load wsn_data/burnin__120531_123429_wsn_d16_troff-1-9_reject4_t4adpt50_mxk100_nssub10000_nsinit70000_m2-0_sd2-104772e+05.mat ...
    t s SAMP SAMPBURNIN s_burnin last_good_w w avgprhmcaccept idx_restart
  
end

if s > ninitsamp
  ss = reshape(SAMP(:,1:7e4), [dim, 7000, 10]);
  ss = ss(:, 1:initPerRestart, :);
  SAMP(:) = 0;
  SAMP(:,1:ninitsamp) = reshape(ss, dim, ninitsamp);
  s = ninitsamp;
end

% load data/tmp_meg_mca_burnin.mat
% % load data/tmp_long_run_for_bica_D6_N300.mat
% maxncompo = 100;
% REJECPROP = 1;
% % itvsave = 1e5;
% % mxit = 2e5;
SAMP = [SAMP, zeros(dim, mxit*2 + ninitsamp - size(SAMP,2))];
% INTG = [INTG, zeros(1, mxit*2 + ninitsamp - length(INTG))];

%% fname
fname = sprintf('wsn_d%d_troff%2.1f_reject%d_t4adpt%d_mxk%d_nssub%d_nsinit%d_m%1.1f_sd%d',...
    dim,troff,REJECPROP,time4adapt,maxncompo,nsubsamp,ninitsamp,m,seed);
fname = strrep(fname,'.','-');
fname = appendfilehead(fname);
fname = strcat('_',fname);

% save(['data/burnin_',fname]);


%% first dpgm update
% ixsubsamp =  iterburnin + ceil(rand(1,nsubsamp)*(s-iterburnin));
% % ixsubsamp = 1:ceil(s/nsubsamp):s;
% % dpgm = dpgaussmixture(SAMP(:,ixsubsamp),maxncompo,dpgmalgo, REJECPROP == 4);

if REJECPROP ~= 5
  dpgm = dpgaussmixture(SAMP(:,1:s),nsubsamp,maxncompo,dpgmalgo,REJECPROP == 4);
%   dpgm = dpgaussmixture(SAMP(:,1:s),nsubsamp,maxncompo,dpgmalgo);
else
  dpgm = dpgmLaplace('data/data/data_wsn_N8_modes.mat');
  dpgm.ixsub = ceil(linspace(1, s, min(nsubsamp, s)));
end

% Choose tgradius
[~,dist_tg_vec] = isinregionL(SAMP(:,dpgm.ixsub), dpgm.mu, dpgm.L, tgradius);
dhist = min(dist_tg_vec, [], 1);
tgradius = quantile(dhist, trquant);

offset = getmodeoffset(llhfunc,dpgm,tgradius);
[maxfreqlogwx, quant90] = histwx(SAMP(:,dpgm.ixsub)',llhfunc,dpgm,offset,tgradius,2);
% m = exp(quant90);
% avgmaxfreqwx = (1-(1/t))*avgmaxfreqwx + (1/t)*maxfreqlogwx;
avgmaxfreqwx = maxfreqlogwx;
% lastupdate = s;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% sampling
wcurr = last_good_w;
p = 0;
v = 0;
SAMP(:,s) = wcurr;
ISREGEN = zeros(mxit*2,1);
REGEN_S = zeros(1,ceil(mxit/3));
rateregen = zeros(1,ceil(mxit/3));
ratehopaccept = zeros(1,ceil(mxit/3));
iteratadapt = zeros(1,ceil(mxit/3));

dpgmk = zeros(1,ceil(mxit/3));

NREJECT = zeros(size(REGEN_S));
isregen = 0;
numhmcaccept = 0;
numhopaccept = 0;
numindepsamp = 0;
numregenafteradapt = 0;
numhopacceptafteradapt = 0;
numreject = 0;
numregen = 0;
numadapt = 0;
numhmc = 0;
prregensum = 0;
prhopsum = 0;
numintg = 0;
elapsed = 0;
iterafterupdate = 0;
lastupdate = s;


% Initialize timing measurements
% Measure HMC unit time
fprintf('Testing HMC unit time...\n');
w = last_good_w;
t_hmc = tic;
Nhmc = 4e3;
for i = 1:Nhmc
  if mod(i, 1e3) == 0
    fprintf('Iter: %d/%d\n', i, Nhmc);
  end
  w = hmc_step_wsn(w, hmc_params);
end
time_hmc = toc(t_hmc);
time_hmc_unit = time_hmc / Nhmc;

prregensum_total = 0;
prhopsum_total = 0;
rateregen_total = zeros(1,ceil(mxit/3));
ratehopaccept_total = zeros(1,ceil(mxit/3));
time_adapt = zeros(1,ceil(mxit/3));
time_adapt_ex_adp = zeros(1,ceil(mxit/3)); % Exclude the time for retraining and independence sampling

t_last_update = tic;
t_total = tic;
time_total = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % dist_tgs = zeros(mxit, 1);
for t = 1 : mxit
    
    % 1st KERNEL, HMC
%     [whmc,hmcaccept,~] = hmcfunc(wcurr,llhfunc,gradfunc,hmc);
%     [whmc, hmcaccept] = hmc_step(wcurr, llhfunc, gradfunc, hmc_params);
    [whmc, hmcaccept] = hmc_step_wsn(wcurr, hmc_params);
    
    numhmc = numhmc + 1;
    numhmcaccept = numhmcaccept + hmcaccept;

%     isintg = any(isinregionL(whmc,dpgm.mu,dpgm.L,tgradius));
    [isintg_vec, dist_tg_vec] = isinregionL(whmc,dpgm.mu,dpgm.L,tgradius);
    isintg = any(isintg_vec);
% %     dist_tgs(t) = min(dist_tg_vec);
    numintg = numintg + isintg;

    % 2nd KERNEL, DPMM
    if ~isintg

        s = s + 1;
        SAMP(:,s) = whmc;
        INTG(:,s) = isintg;

    else

        [wdpgm,ishopaccept,prhop_t,isregen,prregen_t,numreject_t] = indepsampler_dpgm(...
                    whmc,llhfunc,dpgm,m,offset,avgmaxfreqwx,REJECPROP,tgradius);

        numreject = numreject + numreject_t;
        numhopaccept = numhopaccept + ishopaccept;
        numindepsamp = numindepsamp + 1;
        numregen = numregen + isregen;
        prregensum = prregensum + prregen_t;
        prhopsum = prhopsum + prhop_t;
        prregensum_total = prregensum_total + prregen_t;
        prhopsum_total = prhopsum_total + prhop_t;

        if isregen

            ISREGEN(s) = 1;
            REGEN_S(numregen) = s;

            s = s + 1;
            SAMP(:,s) = whmc;
            INTG(:,s) = 1;

            % do adaptation
            iterafterupdate = s - lastupdate;
            % if nsamp2adapt < iterafterupdate
%             t2 = cputime;
            elapsed = toc(t_last_update);
%             elapsed = t2 - t_last_update;

            %% adaptation
            if time4adapt < elapsed && ~dpgm.uniform % when not using darting, adapt
                
                % adapt DPGM
%                 if s > nsubsamp
%                     ixsubsamp = ceil(rand(1,nsubsamp)*s);
%                 else
%                     ixsubsamp = 1:s;
%                     % ixsubsamp = 1:ceil(s/nsubsamp):s;
%                 end
% %                 ixsubsamp = ceil((1:min(nsubsamp,s)) / min(nsubsamp,s) * s);
% %                 dpgm = dpgaussmixture(SAMP(:,ixsubsamp),maxncompo,dpgmalgo,REJECPROP == 4);
                dpgm = dpgaussmixture(SAMP(:,1:s),nsubsamp,maxncompo,dpgmalgo,dpgm.uniform,dpgm);
                
                % update tgradius
                [~,dist_tg_vec] = isinregionL(SAMP(:,dpgm.ixsub), dpgm.mu, dpgm.L, tgradius);
                dhist = min(dist_tg_vec, [], 1);
                tgradius = quantile(dhist, trquant);
                
                offset = getmodeoffset(llhfunc,dpgm,tgradius);
                [maxfreqlogwx, quant90] = histwx(SAMP(:,dpgm.ixsub)',llhfunc,dpgm,offset,tgradius,2);
%                 m = exp(quant90);
                avgmaxfreqwx = maxfreqlogwx;
                lastupdate = s;

%                 set(0,'currentfigure',fig.logC);
%                 plot(numadapt,avgmaxfreqwx,'k*'); hold on;

                % sample independently from Q(.)
                fprintf('regen and indep. sampling..')
                if REJECPROP == 0
                  [wdpgm,~,~,~,~,numreject_t] = indepsampler_dpgm(...
                            wcurr,llhfunc,dpgm,m,offset,avgmaxfreqwx,3,tgradius);
                elseif REJECPROP == 1
                  [wdpgm,~,~,~,~,numreject_t] = indepsampler_dpgm(...
                              wcurr,llhfunc,dpgm,m,offset,avgmaxfreqwx,2,tgradius);
                elseif REJECPROP == 4 || REJECPROP == 5
                   % do nothing
                else
                  error(fprintf('undefined REJECPROP: %d', REJECPROP));
                end
                fprintf('done.\n');
                NREJECT(numregen) = numreject_t;
                
                % Record time
                numadapt = numadapt + 1;

                time_total = toc(t_total);
                rateregen_total(numadapt) = prregensum_total;
                ratehopaccept_total(numadapt) = prhopsum_total;

                time_adapt(numadapt) = time_total;
                if numadapt > 1
                  time_adapt_ex_adp(numadapt) = time_adapt_ex_adp(numadapt-1) + elapsed;
                else
                  time_adapt_ex_adp(numadapt) = elapsed;
                end
                rateregen(numadapt) = prregensum/elapsed;
                ratehopaccept(numadapt) = prhopsum/elapsed;
                iteratadapt(numadapt) = t;
                dpgmk(numadapt) = dpgm.k;

                % Reset
                iterafterupdate = 0;
                prregensum = 0;
                prhopsum = 0;
                t_last_update = tic;
                
            end
            
        end

        s = s + 1;
        SAMP(:,s) = wdpgm;
        INTG(:,s) = 1;

    end % end of if ~isintg
    wcurr = SAMP(:,s);
    
    
    elapsed = toc(t_last_update);

    if dpgm.uniform && time4adapt < elapsed
      % Record time
      numadapt = numadapt + 1;

      time_total = toc(t_total);
      rateregen_total(numadapt) = prregensum_total;
      ratehopaccept_total(numadapt) = prhopsum_total;

      time_adapt(numadapt) = time_total;
      if numadapt > 1
        time_adapt_ex_adp(numadapt) = time_adapt_ex_adp(numadapt-1) + elapsed;
      else
        time_adapt_ex_adp(numadapt) = elapsed;
      end
      rateregen(numadapt) = prregensum/elapsed;
      ratehopaccept(numadapt) = prhopsum/elapsed;
      iteratadapt(numadapt) = t;
      dpgmk(numadapt) = dpgm.k;

      % Reset
      iterafterupdate = 0;
      prregensum = 0;
      prhopsum = 0;
      t_last_update = tic;
    end
    
    
    
    
    
    

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% save

    if ~rem(t+itvsave-1,itvsave) && p > 0
        v = v + 1;
        saveas(fig.regenrate,strcat('data/',fname,'_RATE.png'),'png');
        saveas(fig.regenratevsit,strcat('data/',fname,'_RATEvsIt.png'),'png');
        saveas(fig.hist3,strcat('data/',fname,'_H3.png'),'png');
        disp('---------------- SAVE COMPLETE! ----------------');
%         save(fname,'t','s','mog','dpgm','offset','maxfreqlogwx','lastupdate','SAMP',.....
%                                  'ratehopaccept','rateregen','numadapt','REGEN_S','ISREGEN',... ...
%                                  'numhmc','numreject','numintg','numhopaccept','numindepsamp',...
%                                  'numregen','hmcacceptrate','iteratadapt','time4adapt
        save(['data/',fname]);
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% plot

    if ~rem(t+itvplot-1,itvplot)
        p = p + 1;
        if numadapt > 0
            set(0,'CurrentFigure',fig.regenrate);
%             plot(iteratadapt(1:numadapt),ratehopaccept(1:numadapt),'bo-');
%             plot(iteratadapt(1:numadapt),rateregen(1:numadapt),'rs-');
            plot(iteratadapt(1:numadapt),...
              ratehopaccept_total(1:numadapt) ./ (time_adapt(1:numadapt) / time_hmc_unit),'bo-');  hold on
            plot(iteratadapt(1:numadapt),...
              rateregen_total(1:numadapt) ./ (time_adapt(1:numadapt) / time_hmc_unit),'rs-');  hold off
            title(strrep(fname,'_','-'));
            drawnow;
        
            set(0,'CurrentFigure',fig.regenratevsit);
%             plot(iteratadapt(1:numadapt),ratehopaccept(1:numadapt),'bo-');
%             plot(iteratadapt(1:numadapt),rateregen(1:numadapt),'rs-');
            plot(iteratadapt(1:numadapt),...
              ratehopaccept_total(1:numadapt) ./ iteratadapt(1:numadapt),'bo-');  hold on
            plot(iteratadapt(1:numadapt),...
              rateregen_total(1:numadapt) ./ iteratadapt(1:numadapt),'rs-');  hold off
            title(strrep(fname,'_','-'));
            drawnow;
        end
        fname
        elapsed
        iterafterupdate

        sprintf('t:%d, s:%d\nnumhmc:%d\nnumreject:%d\nnumintg:%d\nnumhopaccept:%d\nnumindepsamp:%d\nnumregen:%d\n',...
            t,s,numhmc,numreject,numintg,numhopaccept,numindepsamp,numregen)
        hmcacceptrate = numhmcaccept / numhmc

        set(0,'currentfigure',fig.hist3); clf
        for ff=1:numfig
            subplot(figrow,figrow,ff);
            ii = cvpair(ff,1);
            jj = cvpair(ff,2);
            [CU,HU] = hist3image(SAMP(ii,1:s),SAMP(jj,1:s),40);
            imagesc(CU{1},CU{2},-HU); axis xy; colormap hot; hold on;
            
            for kk=1:dpgm.k
                plot(dpgm.mu(ii,kk),dpgm.mu(jj,kk),'bo','linewidth',2,'markersize',10);
                plotGauss(dpgm.mu(ii,kk),dpgm.mu(jj,kk),...
                     dpgm.cv(ii,ii,kk),dpgm.cv(jj,jj,kk),dpgm.cv(ii,jj,kk),...
                     2,'--r',2);
            end
            plot(wcurr(ii),wcurr(jj),'mp','markersize',10,'linewidth',2);
            hold off
%             axis(axax(ff,:));
        end
        title(strrep(fname,'_','-'));
        
% %         figure(501); plot(dist_tgs(1:t));
        
        drawnow;
    end

end

fname = fname(2:end);
v = v + 1;
saveas(fig.regenrate,strcat('data/',fname,'_RATE.png'),'png');
saveas(fig.hist3,strcat('data/',fname,'_H3.png'),'png');
save(['data/',fname]);
