function run_drmcmc_wsn_parallel (seed,dim,troff,REJECPROP,m,maxncompo,...
  ntotinitsamp,totnburnin,nrestart,SAMP4ADAPT,nsubsamp,mxit,lfn,lfsize,itvplot,itvprint,itvsave,PLOT)

% clear
% close all
dbstop if error
format compact
% addpath /Documents/00_Research/121004_AISTAT13_DartRegMCMC/package/vdpgm
% addpath('../package/vdpgm');
% addpath('00_DartRegCore');
% addpath('../00_wsn_data');
addpath('../../from_sungjin');

if isdeployed
  
  seed = str2double(seed);
  dim = str2double(dim);
  troff = str2double(troff);
  tgradius = sqrt(dim-1) + troff;
  REJECPROP = str2double(REJECPROP);
  m = str2double(m);
  maxncompo = str2double(maxncompo);
  ntotinitsamp = str2double(ntotinitsamp);
  totnburnin = str2double(totnburnin);
  SAMP4ADAPT = str2double(SAMP4ADAPT);
  nsubsamp = str2double(nsubsamp);
  mxit = str2double(mxit);
  hmc.lfn = str2double(lfn);
  hmc.lfsize = str2double(lfsize);
  itvplot = str2double(itvplot);
  itvsave = str2double(itvsave);
  itvprint = str2double(itvprint);
  PLOT = str2double(PLOT);
  
end

nparallel = 1;
dpgmalgo = 1;
domixchain = 1;
domodesearch = true;
samp4adapt_inc = 1.00;
trquant = .5;

%% Init Parallelization
% if nparallel > 1

sz.pool = matlabpool('size');

if sz.pool > 0 && sz.pool < nparallel && nparallel ~= 0

  matlabpool close
  matlabpool(nparallel);

end

if sz.pool == 0 && nparallel ~= 0

  matlabpool(nparallel);

end
  
% end

RandStream.setDefaultStream(RandStream('mt19937ar','Seed',seed)); %#ok<SETRS>


%% load

hmc = struct('M', lfn, 'eta', lfsize, 'L',1);

if dim == 6
  
  ld = load('../00_wsn_data/data_wsn_N6.mat', 'Xs','Xb', 'Yb', 'Ys', 'N', 'R', 'sig');
  
elseif dim == 8
  
  ld = load('../00_wsn_data/data_wsn_N8.mat', 'Xs','Xb', 'Yb', 'Ys', 'N', 'R', 'sig');
  gt = load('../00_wsn_data/gt.mat','gt_mean','gt_cov');
  
end

hmc.Xb = ld.Xb;
hmc.Yb = ld.Yb;
hmc.Ys = ld.Ys;
hmc.N = ld.N;
hmc.R = ld.R;
hmc.sig = ld.sig;

gt.mu = gt.gt_mean;
gt.cov = gt.gt_cov;

%% dim
nnode = ld.N;
dim = ld.N * 2;
tgradius = ones(nparallel, 1) * (sqrt(dim-1) + troff);


%% get function handles
[llhfunc, gradfunc] = funs_wsn(ld.Xb, ld.Yb, ld.Ys, ld.N, ld.R, ld.sig);
% clear ld


%% fname
fname = sprintf('wsn_para_d%d_troff%2.1f_reject%d_s4adpt%d_mxk%d_nssub%d_nsinit%d_m%1.1f_sd%d',...
  dim,troff,REJECPROP,SAMP4ADAPT,maxncompo,nsubsamp,ntotinitsamp,m,seed);
fname = strrep(fname,'.','-');
fname = appendfilehead(fname);
fname = strcat('_',fname);


%% plots
for j = 1:nparallel
  fig.hist3(j) = figure(440+j); clf;
end

fig.rerror_mu = figure(450); clf;
fig.rerror_cov = figure(451); clf;
fig.R = figure(452); clf;
fig.locerr = figure(453); clf;

if PLOT && ~isdeployed
  for j = 1:nparallel
    set(fig.hist3(j),'windowstyle','docked');
  end
  set(fig.rerror_mu,'windowstyle','docked');
  set(fig.rerror_cov,'windowstyle','docked');
  set(fig.R,'windowstyle','docked');
  set(fig.locerr,'windowstyle','docked');
  
end

pause(0.5);

% nfig = figrow^2;
nfig = 1;
cvpair = [1:nfig; mod((1:nfig) + dim/2 -1, dim)+1]';

nburnin = ceil(totnburnin / nrestart);
totnburnin = nburnin * nrestart;
ninitsamp =ceil(ntotinitsamp / nrestart);
ntotinitsamp = ninitsamp * nrestart;
mxit = max(mxit, ntotinitsamp);

SAMPBURNIN = zeros(dim, ninitsamp*nrestart*nparallel);
SAMP = cell(nparallel, 1);
INTG = cell(nparallel, 1);
TOUR = cell(nparallel, 1);
for qq = 1 : nparallel
  SAMP{qq} = zeros(dim, mxit*2);
  INTG{qq} = zeros(mxit*2, 1);
  TOUR{qq} = zeros(dim, ninitsamp);
end
w = zeros(dim,nparallel);
last_good_w = zeros(dim,nparallel);
avgprhmcaccept = zeros(nparallel,1);
prhmcaccept = zeros(nparallel,1);
% s_burnin = zeros(nparallel,1);
% s = zeros(nparallel,1);

RR = zeros(ceil(mxit/itvplot)+100,1);
REMU = zeros(ceil(mxit/itvplot)+100,1);
RECV = zeros(ceil(mxit/itvplot)+100,1);
ELOC = zeros(ceil(mxit/itvplot)+100,1);
S = zeros(nparallel, ceil(mxit/itvplot)+100);
TIME = zeros(ceil(mxit/itvplot)+100,1);

p = 0;
v = 0;

time_start = tic;
time_dpgm = 0;

%% mode search
if domodesearch
  
  %preliminary run to build initial dpmm
  disp('start prerun')
  std_init = 2;
  h = 0;
  ntour = 0;
  ntotinittour = nparallel*nrestart;
  
  while ntour < ntotinittour
    
    % idx_restart = idx_restart + 1;
    
    % Restart
    w = (rand(dim, nparallel)-.5)*std_init+.5;
    avgprhmcaccept = zeros(nparallel,1);
    
    parfor qq = 1:nparallel  % mode search
%     for qq = 1:nparallel      
      
      for t = 1 : nburnin + ninitsamp
        
        [w(:,qq), ~, prhmcaccept(qq)] = hmc_step_wsn(w(:,qq), hmc);
        prhmcaccept(qq) = min(1, prhmcaccept(qq));
        avgprhmcaccept(qq) = (1-1/t) * avgprhmcaccept(qq)+ 1/t * prhmcaccept(qq);
        
        if t > nburnin
          TOUR{qq}(:, t - nburnin) = w(:, qq);
        end

        %% plot
        itv = itvplot;
        if ~rem(t+itv,itv) && qq <= 2
            fprintf('%d - ntour:%d/%d: Burn-in iter: %d, accept rate: %f\n', qq, ntour, ntotinittour, t, avgprhmcaccept(qq));
        end
      
      end

      last_good_w(:,qq) = w(:,qq);
    end
    t = nburnin + ninitsamp;
    
%     itv = itvplot;
%       
%     %% plot
%     if ~rem(t+itv,itv)
% 
%       for qq = 1:2
% 
%         fprintf('%d - ntour:%d/%d: Burn-in iter: %d, accept rate: %f\n', qq, ntour, ntotinittour, t, avgprhmcaccept(qq));
%         set(0,'CurrentFigure',fig.hist3(qq)); clf;
% 
%         ss = reshape(TOUR{qq}(:,1:u), [nnode, 2, u]);
%         ss = permute(ss,[2 1 3]);
%         ss = ss(:,:);
%         [CU,HU] = hist3image(ss(1,1:(nnode*u)),ss(2,1:(nnode*u)),70);
%         imagesc(CU{1},CU{2},-HU); axis xy; colormap hot; hold on;
% 
%         % draw base stations
%         for nn = 1:3
%           plot(hmc.Xb(nn,1),hmc.Xb(nn,2),'ks','linewidth',2,'markersize',10);
%         end
%         % draw node
%         for i = 1:nnode
%           plot(w(i,qq),w(i+nnode,qq),'ro','linewidth',2,'markersize',10);
%           plot(ld.Xs(i,1),ld.Xs(i,2),'kx','linewidth',2,'markersize',10);
%         end
% 
%         hold off
%         axis([-0.1 1 -0.3 1])
%         drawnow
% 
%       end
%     end
    
    %% combine valid tours and stack up in the SAMPBURNIN
%     nvalidtour = sum(avgprhmcaccept > .3);
    for qq = find(avgprhmcaccept' > .3)
      SAMPBURNIN(:, h + (1 : ninitsamp)) = TOUR{qq};
      h = h + ninitsamp;
      ntour = ntour + 1;
    end
%     SAMPBURNIN(:,h+1:h+nvalidtour*ninitsamp) = VTOUR(:,:);
%     h = h+nvalidtour*ninitsamp;
%     ntour = ntour + nvalidtour;

  end
  
  %% Set sample index.
  s_init = ninitsamp*nrestart;
  s = s_init;
  for qq = 1 : nparallel
    SAMP{qq}(:, 1 : s) = SAMPBURNIN(:, s * (qq - 1) + (1:s));
  end
  % SAMP(:, 1:s, :) = reshape(SAMPBURNIN(:,1:s*nparallel), [dim ninitsamp*nrestart nparallel]);
  
%   save ../00_wsn_data/prelimrun_4qq_1nrestart.mat s s_init SAMP last_good_w w avgprhmcaccept
  save ../00_wsn_data/prelimrun_tmp.mat s s_init SAMP last_good_w w avgprhmcaccept
  %         save prelimrun_2qq_allmode.mat s s_init SAMP last_good_w w avgprhmcaccept idx_restart
  %         save prelimrun_2qq.mat s s_init SAMP last_good_w w avgprhmcaccept idx_restart
  
  
  %% plot
  for qq = 1:min(2, nparallel)
    
    set(0,'CurrentFigure',fig.hist3(qq)); clf;
    
    ss = reshape(SAMP{qq}(:,1:s), [nnode, 2, s]);
    ss = permute(ss,[2 1 3]);
    ss = ss(:,:);
    [CU,HU] = hist3image(ss(1,:),ss(2,:),70);
    imagesc(CU{1},CU{2},-HU); axis xy; colormap hot; hold on;
    
    % draw base stations
    for nn = 1:3
      plot(hmc.Xb(nn,1),hmc.Xb(nn,2),'ks','linewidth',2,'markersize',10);
    end
    
    for i = 1:nnode
      plot(ld.Xs(i,1),ld.Xs(i,2),'kx','linewidth',2,'markersize',10);
      plot(w(i,qq),w(i+nnode,qq),'ro','linewidth',2,'markersize',10);
    end
    
    hold off
    axis([-0.1 1 -0.3 1])
    
    drawnow
    
  end
  
else
  
  %         load prelimrun_2qq
  %         load prelimrun_2qq_allmode.mat
  load ../00_wsn_data/prelimrun_4qq_1nrestart
%   load ../00_wsn_data/prelimrun_4qq_allmode.mat
  %         load 00_wsn_data/burnin__120531_123429_wsn_d16_troff-1-9_reject4_t4adpt50_mxk100_nssub10000_nsinit70000_m2-0_sd2-104772e+05.mat ...
  %   t s s_init SAMP SAMPBURNIN s_burnin last_good_w w avgprhmcaccept idx_restart
  
  assert(s == ninitsamp * nrestart);
  
  if ~iscell(SAMP) 
    SAMP_bak = SAMP;
    SAMP = cell(size(SAMP_bak,3), 1);
    for qq = 1 : size(SAMP_bak, 3)
      SAMP{qq} = SAMP_bak(:, :, qq);
    end
    clear SAMP_bak
  end    
  if nparallel > length(SAMP)
    warning('nparallel is larger than length(SAMP)');
  end
  SAMP = SAMP(mod(0 : nparallel-1, size(SAMP, 3)) + 1);
  avgprhmcaccept = avgprhmcaccept(mod(0 : nparallel-1, length(avgprhmcaccept)) + 1);
  last_good_w = last_good_w(:, mod(0 : nparallel-1, size(last_good_w, 2)) + 1);
  w = w(:, mod(0 : nparallel-1, size(w, 2)) + 1);
end

% combine sample

CSAMP = zeros(dim, nparallel*ninitsamp*nrestart);
for qq = 1 : nparallel
  CSAMP(:, s * (qq - 1) + (1:s)) = SAMP{qq}(:, 1 : s);
end
% CSAMP = reshape(SAMP(:, 1:s, :), [dim, nparallel*ninitsamp*nrestart]);
lencombsamp = size(CSAMP,2);


time_prerun = toc(time_start);


%% first dpgm update
parfor qq=1:nparallel  % first dpgm update
% for qq=1:nparallel
  
  if domixchain
    
%     if lencombsamp < nsubsamp
%       % ixsubsamp = ceil(rand(1,lencombsamp)*lencombsamp);
%       ixsubsamp = 1:lencombsamp;
%     else
%       % ixsubsamp = ceil(rand(1,nsubsamp)*lencombsamp);
%       % thinning
%       ixsubsamp = round(linspace(1, lencombsamp, nsubsamp));
%     end
    
%     dpgm(qq) = dpgaussmixture(CSAMP(:,ixsubsamp),maxncompo,dpgmalgo);
% %     tic_dpgm = tic;
    if REJECPROP ~= 5
%       dpgm(qq) = dpgaussmixture(CSAMP(:,ixsubsamp),nsubsamp,maxncompo,dpgmalgo,REJECPROP == 4);
      dpgm(qq) = dpgaussmixture(CSAMP,nsubsamp,maxncompo,dpgmalgo,REJECPROP == 4);
    else
      dpgm(qq) = dpgmLaplace('../../data/data_wsn_N8_modes.mat');
      dpgm(qq).ixsub = ceil(linspace(1, lencombsamp, min(nsubsamp, lencombsamp)));
    end
    
    % Choose tgradius
    [~,dist_tg_vec] = isinregionL(CSAMP(:,dpgm(qq).ixsub), dpgm(qq).mu, dpgm(qq).L, tgradius(qq));
    dhist = min(dist_tg_vec, [], 1);
    tgradius(qq) = quantile(dhist, trquant);
    
    offset(qq) = getmodeoffset(llhfunc,dpgm(qq),tgradius(qq));
    maxfreqlogwx(qq) = histwx(CSAMP(:,dpgm(qq).ixsub)',llhfunc,dpgm(qq),offset(qq),tgradius(qq),2);
    % maxfreqlogwx(qq) = histwxY(CSAMP(:,ixsubsamp(qq,:))',llhfunc,dpgm(qq),offset(qq),tgradius(qq),2);
% %     time_dpgm = time_dpgm + toc(tic_dpgm);
    
  else
    
%     if s < nsubsamp
%       % ixsubsamp = ceil(rand(1,lencombsamp)*lencombsamp);
%       ixsubsamp = 1:s;
%     else
%       % ixsubsamp = ceil(rand(1,nsubsamp)*lencombsamp);
%       % thinning
% %       ixsubsamp = qq:ceil((s-qq)/nsubsamp):s;
%       ixsubsamp = round(linspace(1, s, nsubsamp));
%     end
    
% %     tic_dpgm = tic;
%     dpgm(qq) = dpgaussmixture(SAMP{qq}(:,ixsubsamp),maxncompo,dpgmalgo);
    if REJECPROP ~= 5
%       dpgm(qq) = dpgaussmixture(SAMP{qq}(:,ixsubsamp),nsubsamp,maxncompo,dpgmalgo,REJECPROP == 4);
      dpgm(qq) = dpgaussmixture(SAMP{qq}(:,1:s),nsubsamp,maxncompo,dpgmalgo,REJECPROP == 4);
    else
      dpgm(qq) = dpgmLaplace('../../data/data_wsn_N8_modes.mat');
      dpgm(qq).ixsub = ceil(linspace(1, s, min(nsubsamp, s)));
    end
    
    % Choose tgradius
    [~,dist_tg_vec] = isinregionL(SAMP{qq}(:,dpgm(qq).ixsub), dpgm(qq).mu, dpgm(qq).L, tgradius(qq));
    dhist = min(dist_tg_vec, [], 1);
    tgradius(qq) = quantile(dhist, trquant);
    
    offset(qq) = getmodeoffset(llhfunc,dpgm(qq),tgradius(qq));
    maxfreqlogwx(qq) = histwx(SAMP{qq}(:,dpgm(qq).ixsub)',llhfunc,dpgm(qq),offset(qq),tgradius(qq),2);
% %     time_dpgm = time_dpgm + toc(tic_dpgm);
    
  end
  
end

disp('first dpmm update done!')

time_firstupdate = toc(time_start);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sampling
wcurr = zeros(dim, nparallel);
for qq = 1 : nparallel
  wcurr(:, qq) = SAMP{qq}(:, s);
end
% wcurr = SAMP(:,s,:);


% s = 0;
% SAMP(:,s,:) = wcurr;
% ISREGEN = zeros(1,2*mxit);
% REGEN_S = zeros(1,ceil(mxit/20));
% rateregen = zeros(1,ceil(mxit/20));
% rateregen2 = zeros(1,ceil(mxit/20));
% ratehopaccept = zeros(1, ceil(mxit/20));
% ratehopaccept2 = zeros(1,ceil(mxit/20));
% iteratadapt = zeros(1, ceil(mxit/20));
% itaftadapt = zeros(1,ceil(mxit/20));
% timeatadapt = zeros(1, ceil(mxit/20));
% NREJECT = zeros(size(REGEN_S));
hmcaccept = zeros(1, nparallel);
nhmcaccept = zeros(1, nparallel);
whmc = zeros(dim, nparallel);
isintg = false(nparallel, 1);

% return from independent sampler
wdpgm = zeros(dim, nparallel);
ishopaccept = zeros(1, nparallel);
prhop_t = zeros(1, nparallel);
isregen = zeros(1, nparallel);
prregen_t = zeros(1, nparallel);
nreject = zeros(1, nparallel);
nreject_t = zeros(1, nparallel);
nhopaccept = zeros(1, nparallel);
sumprregen = zeros(1, nparallel);
sumprhopaccept = zeros(1, nparallel);
nindepsamp = zeros(1, nparallel);
nadapt = zeros(1, nparallel);
nhmc = zeros(1, nparallel);
nregen = zeros(1, nparallel);
nintg = zeros(1, nparallel);
% iterafteradapt = zeros(1, nparallel);
iterlastregen = zeros(1,nparallel);
% iterafteradapt = zeros(1,nparallel);
% ntours = zeros(1,nparallel);
% iterlastregen = codistributed.zeros(1, nparallel);
lastupdate = ones(1, nparallel) * s;
samp4adapt = ones(1, nparallel) * SAMP4ADAPT;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic;

ts = zeros(nparallel, 1);
s = ones(nparallel, 1) * s;  % s is a vector from now on
last_t_plot = 0;

while min(ts) < mxit

  % Sync regeneration every some iterations (one session). If an dpgm
  % update request occurs, stop the current thread to wait the next sync.
  it_sync = max(samp4adapt);
  
  parfor qq = 1 : nparallel  % main loop
%   for qq = 1 : nparallel
    
    % Continued from previous session.
    if domixchain && isregen(qq) && ~dpgm(qq).uniform  % update dpgm after a sync
      %%         iterafteradapt(qq) = s - lastupdate(qq);
      %%         iterlastregen(qq) = s;
      
%       if lencombsamp < nsubsamp
%         % ixsubsamp =  ceil(rand(1,lencombsamp)*lencombsamp);
%         ixsubsamp = 1:lencombsamp;
%       else
%         % ixsubsamp =  ceil(rand(1,nsubsamp)*lencombsamp);
%         % thinning
%         ixsubsamp = round(linspace(1, lencombsamp, nsubsamp));
% %         ixsubsamp = qq:ceil((lencombsamp-qq)/nsubsamp):lencombsamp;
%       end
      
% %       tic_dpgm = tic;
%       dpgm(qq) = dpgaussmixture(CSAMP(:,ixsubsamp),maxncompo,dpgmalgo);
      dpgm(qq) = dpgaussmixture(CSAMP,nsubsamp,maxncompo,dpgmalgo,dpgm(qq).uniform,dpgm(qq));
      
      % update tgradius
      [~,dist_tg_vec] = isinregionL(CSAMP(:,dpgm(qq).ixsub), dpgm(qq).mu, dpgm(qq).L, tgradius(qq));
      dhist = min(dist_tg_vec, [], 1);
      tgradius(qq) = quantile(dhist, trquant);
      
      offset(qq) = getmodeoffset(llhfunc,dpgm(qq),tgradius(qq));
      maxfreqlogwx(qq) = histwx(CSAMP(:,dpgm(qq).ixsub)',llhfunc,dpgm(qq),offset(qq),tgradius(qq),2);
% %       time_dpgm = time_dpgm + toc(tic_dpgm);
      
      nadapt(qq) = nadapt(qq) + 1;
      lastupdate(qq) = s(qq);
%       iterafteradapt(qq) = 0;
      samp4adapt(qq) = floor(samp4adapt(qq)*samp4adapt_inc);
      
      fprintf('DPMM adapted! id:%d t:%d, SAMP4ADAPT:%d\n',qq,ts(qq),samp4adapt(qq))
      
%       % sample independently from Q(.)
%       if REJECPROP % sample from  min{p(x), mf(x)}
%         
%         [wdpgm(:,qq),~,~,~] = indepsampler_dpgm(...
%           wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),2,tgradius(qq));
%         
%       else % sample from f(x) and accept by min{1,w(y)/C}
%         
%         [wdpgm(:,qq),~,~,~] = indepsampler_dpgm(...
%           wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),3,tgradius(qq));
%         
%       end
      
      % sample independently from Q(.)
      if REJECPROP == 0 % sample from f(x) and accept by min{1,w(y)/C}
        [wdpgm(:,qq),~,~,~,~,numreject_t] = indepsampler_dpgm(...
          wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),3,tgradius(qq));
      elseif REJECPROP == 1 % sample from  min{p(x), mf(x)}
        [wdpgm(:,qq),~,~,~,~,numreject_t] = indepsampler_dpgm(...
          wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),2,tgradius(qq));
      elseif REJECPROP == 4 || REJECPROP == 5
        % do nothing
      else
        error(fprintf('undefined REJECPROP: %d', REJECPROP));
      end
      
      wcurr(:,qq) = wdpgm(:,qq);
      
      SAMP{qq}(:,s(qq)) = wcurr(:,qq);
      
      isregen(qq) = 0;
    end  % if domixchain && isregen(qq)  % update dpgm after a sync

    % Start a new session.
    session_end = ts(qq) + it_sync;
    while ts(qq) < session_end
      ts(qq) = ts(qq) + 1;
      s(qq) = s(qq) + 1;
      
      % 1st KERNEL, HMC
      [whmc(:,qq), hmcaccept(qq)] = hmc_step_wsn(wcurr(:,qq), hmc);
      %                 [whmc(:,qq), hmcaccept(qq)] = sample_RWM_wsn(wcurr(:,qq), 0.0001, llhfunc);
      nhmc(qq) = nhmc(qq) + 1;
      nhmcaccept(qq) = nhmcaccept(qq) + hmcaccept(qq);
%       isintg(qq) = any(isinregion(whmc(:,qq)',dpgm(qq).mu,dpgm(qq).cv,tgradius(qq)));
      isintg(qq) = any(isinregionL(whmc(:,qq),dpgm(qq).mu,dpgm(qq).L,tgradius(qq)));
      nintg(qq) = nintg (qq)+ isintg(qq);
      INTG{qq}(s(qq)) = isintg(qq);
      
      wcurr(:,qq) = whmc(:,qq);
      
      % 2nd KERNEL, DPMM independent sampler
      if isintg(qq)

        % propose from dpmm
        [wdpgm(:,qq),ishopaccept(qq),prhop_t(qq),isregen(qq),prregen_t(qq),nreject_t(qq)] = ...
          indepsampler_dpgm(...
          whmc(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),REJECPROP,tgradius(qq));

        % compute some statistics
        nreject(qq) = nreject(qq) + nreject_t(qq);
        nhopaccept(qq) = nhopaccept(qq) + ishopaccept(qq);
        nindepsamp(qq) = nindepsamp(qq) + 1;
        nregen(qq) = nregen(qq) + isregen(qq);
        sumprregen(qq) = sumprregen(qq) + prregen_t(qq);
        sumprhopaccept(qq) = sumprhopaccept(qq) + prhop_t(qq);

        if ishopaccept(qq)
          
          wcurr(:,qq) = wdpgm(:,qq);

          if (isregen(qq))
            % Regneration occurs.
            
%             iterafteradapt = s(qq) - lastupdate(qq);
            iterlastregen(qq) = s(qq);
            
            if samp4adapt(qq) < s(qq) - lastupdate(qq) && ~dpgm(qq).uniform  % Should update.
              
              SAMP{qq}(:,s(qq)) = wcurr(:,qq);

              if ~domixchain
                % If not mixing, update immediately.
                
%                 if s(qq) < nsubsamp
%                   % ixsubsamp = ceil(rand(1,lencombsamp)*lencombsamp);
%                   ixsubsamp = 1:s(qq);
%                 else
%                   % ixsubsamp = ceil(rand(1,nsubsamp)*lencombsamp);
%                   % thinning
% %                   ixsubsamp = qq:ceil((s(qq)-qq)/nsubsamp):s(qq);
%                   ixsubsamp = round(linspace(1, s(qq), nsubsamp));
%                 end
                
% %                 tic_dpgm = tic;
%                 dpgm(qq) = dpgaussmixture(SAMP{qq}(:,ixsubsamp),maxncompo,dpgmalgo);
                dpgm(qq) = dpgaussmixture(SAMP{qq}(:,1:s(qq)),nsubsamp,maxncompo,dpgmalgo,dpgm(qq).uniform,dpgm(qq));

                % update tgradius
                [~,dist_tg_vec] = isinregionL(SAMP{qq}(:,dpgm(qq).ixsub), dpgm(qq).mu, dpgm(qq).L, tgradius(qq));
                dhist = min(dist_tg_vec, [], 1);
                tgradius(qq) = quantile(dhist, trquant);

                offset(qq) = getmodeoffset(llhfunc,dpgm(qq),tgradius(qq));
                maxfreqlogwx(qq) = histwx(SAMP{qq}(:,dpgm(qq).ixsub)',llhfunc,dpgm(qq),offset(qq),tgradius(qq),2);
% %                 time_dpgm = time_dpgm + toc(tic_dpgm);
                
                nadapt(qq) = nadapt(qq) + 1;
                lastupdate(qq) = s(qq);
%                 iterafteradapt(qq) = 0;
                samp4adapt(qq) = floor(samp4adapt(qq)*1.05);
                
                fprintf('DPMM adapted! id:%d t:%d, SAMP4ADAPT:%d\n',qq,ts(qq),samp4adapt(qq))
                
%                 % sample independently from Q(.)
%                 if REJECPROP % sample from  min{p(x), mf(x)}
%                   
%                   [wdpgm(:,qq),~,~,~] = indepsampler_dpgm(...
%                     wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),2,tgradius(qq));
%                   
%                 else % sample from f(x) and accept by min{1,w(y)/C}
%                   
%                   [wdpgm(:,qq),~,~,~] = indepsampler_dpgm(...
%                     wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),3,tgradius(qq));
%                   
%                 end

                % sample independently from Q(.)
                if REJECPROP == 0 % sample from f(x) and accept by min{1,w(y)/C}
                  [wdpgm(:,qq),~,~,~,~,numreject_t] = indepsampler_dpgm(...
                    wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),3,tgradius(qq));
                elseif REJECPROP == 1 % sample from  min{p(x), mf(x)}
                  [wdpgm(:,qq),~,~,~,~,numreject_t] = indepsampler_dpgm(...
                    wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),2,tgradius(qq));
                elseif REJECPROP == 4 || REJECPROP == 5
                  % do nothing
                else
                  error(fprintf('undefined REJECPROP: %d', REJECPROP));
                end
                
                wcurr(:,qq) = wdpgm(:,qq);

              else
                
                % Otherwise, wait for the next sync.
                % isregen(qq) is true after the loop only in this case.
                break;
                
              end
              
            end  % if samp4adapt(qq) < iterafteradapt  % Should update.
            
          end  % if (isregen(qq)).

        end  % if ~ishopaccept(qq)

      end  % if ~isintg(qq)

      SAMP{qq}(:,s(qq)) = wcurr(:,qq);
      
      isregen(qq) = 0;

    end  % session.
    
  end  % parfor qq = 1 : nparallel.

  % Combine new tours.
  if domixchain
      
    for qq = find(isregen)
      % For all chains that have regeneration AND need to be updated.
      % This does not merge those chains that regeneration happens but not
      % need to update yet.

      ixnewtour = (lastupdate(qq)+1) : s(qq);
      CSAMP = [CSAMP SAMP{qq}(:,ixnewtour)];

    end

    lencombsamp = size(CSAMP,2);

%     % update dpgm
%     parfor qq = 1:nparallel  % update dpgm
% %     for qq = 1:nparallel
% 
%       if isregen(qq)
% 
%         %%         iterafteradapt(qq) = s - lastupdate(qq);
%         %%         iterlastregen(qq) = s;
% 
%         if lencombsamp < nsubsamp
%           % ixsubsamp =  ceil(rand(1,lencombsamp)*lencombsamp);
%           ixsubsamp = 1:lencombsamp;
%         else
%           % ixsubsamp =  ceil(rand(1,nsubsamp)*lencombsamp);
%           % thinning
%           ixsubsamp = qq:ceil((lencombsamp-qq)/nsubsamp):lencombsamp;
%         end
% 
%         dpgm(qq) = dpgaussmixture(CSAMP(:,ixsubsamp),maxncompo,dpgmalgo);
%         offset(qq) = getmodeoffset(llhfunc,dpgm(qq),tgradius(qq));
%         maxfreqlogwx(qq) = histwx(CSAMP(:,ixsubsamp)',llhfunc,dpgm(qq),offset(qq),tgradius(qq),2);
% 
%         nadapt(qq) = nadapt(qq) + 1;
%         lastupdate(qq) = s(qq);
%         iterafteradapt(qq) = 0;
%         samp4adapt(qq) = floor(samp4adapt(qq)*1.05);
% 
%         fprintf('DPMM adapted! id:%d t:%d, SAMP4ADAPT:%d\n',qq,ts(qq),samp4adapt(qq))
% 
%         % sample independently from Q(.)
%         if REJECPROP % sample from  min{p(x), mf(x)}
% 
%           [wdpgm(:,qq),~,~,~] = indepsampler_dpgm(...
%             wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),2,tgradius(qq));
% 
%         else % sample from f(x) and accept by min{1,w(y)/C}
% 
%           [wdpgm(:,qq),~,~,~] = indepsampler_dpgm(...
%             wcurr(:,qq),llhfunc,dpgm(qq),m,offset(qq),maxfreqlogwx(qq),3,tgradius(qq));
% 
%         end
% 
%         wcurr(:,qq) = wdpgm(:,qq);
% 
%         s(qq) = s(qq) + 1;          
%         SAMP{qq}(:,s(qq)) = wcurr(:,qq);
%         INTG{qq}(s(qq)) = isintg(qq);
% 
%         isregen(qq) = 0;
%       end  % if isregen(qq)
% 
%     end  % parfor qq = 1:nparallel  % update dpgm

  end % if domixchain

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% save
  if (floor(last_t_plot/itvsave) ~= floor(min(ts) / itvsave)) && p > 0
%   if ~rem(ts(1)+itvsave,itvsave) && p > 0
    
    v = v + 1;
    
    time_total = toc(time_start);
    
    cd 00_log_working
    
    % PNG
    %                 saveas(fig.hist3(1),strcat(fname,'_HIST1.png'),'png');
    %                 saveas(fig.hist3(2),strcat(fname,'_HIST2.png'),'png');
    %                 saveas(fig.R,strcat(fname,'_R.png'),'png');
    %                 saveas(fig.rerror_mu,strcat(fname,'_REMU.png'),'png');
    %                 saveas(fig.rerror_cov,strcat(fname,'_RECV.png'),'png');
    %
    %                 % FIG
    %                 saveas(fig.hist3(1),strcat(fname,'_HIST1.fig'),'fig');
    %                 saveas(fig.hist3(2),strcat(fname,'_HIST2.fig'),'fig');
    %                 saveas(fig.R,strcat(fname,'_R.fig'),'fig');
    %                 saveas(fig.rerror_mu,strcat(fname,'_REMU.fig'),'fig');
    %                 saveas(fig.rerror_cov,strcat(fname,'_RECV.fig'),'fig');
    
    save(fname);
    % save(fname,'SAMP','s','s_init','seed','dim','lfn','lfsize','nrestart','ninitsamp','nburnin','nsubsamp');
    
    disp('---------------- SAVE COMPLETE! ----------------');
    cd ..
    
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% print
  if (floor(last_t_plot/itvprint) ~= floor(min(ts) / itvprint))
%   if ~rem(ts(1)+itvprint,itvprint)
    
    %                 fname
    % elapsed
    %                 iterafteradapt
    %[ratehopaccept(nadapt(qq)+1) rateregen(nadapt(qq)+1)]
    hmcacceptrate = nhmcaccept./nhmc
    
    for qq=1:min(2, nparallel)
      
      sprintf('qq:%d\n',qq)
      sprintf('t:%d, s:%d\nnhmc:%d\nnreject:%d\nnintg:%d\nnhopaccept:%d\nnindepsamp:%d\nnregen:%d\n',...
        ts(qq),s(qq),nhmc(qq),nreject(qq),nintg(qq),nhopaccept(qq),nindepsamp(qq),nregen(qq))
      
    end
    
  end
  
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% plot
  
  if (floor(last_t_plot/itvplot) ~= floor(min(ts) / itvplot))
%   if ~rem(ts(1)+itvplot,itvplot)
    
    p = p + 1;
    
    S(:, p) = s;
    TIME(p) = toc(time_start);
    
    %% R convergence Diagnostic
    if min(s) > nburnin
      
      % compute R convergence diagnostic
      SAMP_FOR_R = zeros(min(s)-s_init, dim, nparallel);
      for qq = 1 : nparallel
        SAMP_FOR_R(:,:,qq) = SAMP{qq}(:,s_init+1:min(s))';
      end
      RR(p) = mpsrf(SAMP_FOR_R);
      
      %% compute error
      for qq=1:nparallel
        
        sampmu = mean(SAMP{qq}(:,s_init+1:s(qq))');
        sampcov = cov(SAMP{qq}(:,s_init+1:s(qq))');
        ELOC(p,qq) = sum(abs(ld.Xs(:)' - sampmu));
        
        type = '-k';
        REMU(p,qq) = sum(abs(gt.mu - sampmu))/sum(abs(gt.mu));
        set(0, 'currentfigure', fig.rerror_mu);
%         plot(1:p, REMU(1:p,qq), type);
        plot(TIME(1:p), REMU(1:p,qq), type);
        
        RECV(p,qq) = sum(sum(abs(gt.cov - sampcov)))/sum(sum(abs(gt.cov)));
        set(0, 'currentfigure', fig.rerror_cov);
%         plot(1:p, RECV(1:p,qq), type);
        plot(TIME(1:p), RECV(1:p,qq), type);
        
        set(0, 'currentfigure', fig.R);
%         plot(1:p, RR(1:p), type);
        plot(TIME(1:p), RR(1:p), type);
        
        set(0, 'currentfigure', fig.locerr);
%         plot(1:p, ELOC(1:p,qq) , type);
        plot(TIME(1:p), ELOC(1:p,qq) , type);
        
      end
      
    end
    
    drawnow;
    
    %% plot
    
    for qq=1:nparallel
      
      set(0,'currentfigure',fig.hist3(qq)); clf
      
      ss = reshape(SAMP{qq}(:,s_init+1:s(qq)), [nnode, 2, s(qq)-s_init]);
      ss = permute(ss,[2 1 3]);
      ss = ss(:,:);
      [CU,HU] = hist3image(ss(1,:),ss(2,:),70);
      imagesc(CU{1},CU{2},-HU); axis xy; colormap hot; hold on;
      
      % draw base stations
      for nn = 1:3
        plot(hmc.Xb(nn,1),hmc.Xb(nn,2),'ks','linewidth',2,'markersize',10);
      end
      
      for kk = 1:dpgm(qq).k
        
        muk = dpgm(qq).mu(:,kk);
        muk = reshape(muk,[nnode, 2]);
        cvk = dpgm(qq).cv(:,:,kk);
        
        for nn = 1:nnode
          
          plot(muk(nn,1), muk(nn,2), 'bo', 'linewidth', 2, 'markersize', 2);
          plotGauss(muk(nn,1), muk(nn,2),...
            cvk(nn,nn),cvk(nn+nnode,nn+nnode),cvk(nn+nnode,nn),...
            2,'--r',1);
          
        end
        
      end
      
      for i = 1:nnode
        
        plot(ld.Xs(i,1),ld.Xs(i,2),'kx','linewidth',2,'markersize',10);
        plot(wcurr(i,qq),wcurr(i+nnode,qq),'ro','linewidth',2,'markersize',10);
        
      end
      
      hold off
      axis([-0.1 1 -0.3 1])
      % axis(axax(ff,:));
      
      title(strrep(fname,'_','-'));
      drawnow;
      
    end
    
    toc
    tic
    
  end
  
  last_t_plot = min(ts);
end

%% save when finished
time_total = toc(time_start);
fname = fname(2:end);
cd 00_log_finished

% saveas(fig.hist3(1),strcat(fname,'_HIST1.png'),'png');
% saveas(fig.hist3(2),strcat(fname,'_HIST2.png'),'png');
% saveas(fig.R,strcat(fname,'_R.png'),'png');
% saveas(fig.rerror_mu,strcat(fname,'_REMU.png'),'png');
% saveas(fig.rerror_cov,strcat(fname,'_RECV.png'),'png');
%
% saveas(fig.hist3(1),strcat(fname,'_HIST1.fig'),'fig');
% saveas(fig.hist3(2),strcat(fname,'_HIST2.fig'),'fig');
% saveas(fig.R,strcat(fname,'_R.fig'),'fig');
% saveas(fig.rerror_mu,strcat(fname,'_REMU.fig'),'fig');
% saveas(fig.rerror_cov,strcat(fname,'_RECV.fig'),'fig');

save(fname);
cd ..
disp('---------------- RUN COMPLETE! ----------------');
% save(fname,'SAMP','s','s_init','seed','dim','lfnum','lfsize','nrestart','ninitsamp','iterburnin','nsubsamp');
