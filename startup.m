run('~/apps/vlfeat-0.9.20/toolbox/vl_setup')
%run('~/apps/vlg/toolbox/vlg_setup')
run('~/apps/matconvnet-1.0-beta18/matlab/vl_setupnn')

addpath(genpath('~/code/matlab/gitmatlab/utils'))
addpath(genpath('~/code/matlab/gitmatlab/utils/exportfig'))

%addpath(genpath('~/code/matlab/gitmatlab/third-party/gplm'))

%addpath('/Users/pratik/apps/tensor_toolbox_2.6')
%addpath('/Users/pratik/apps/tensor_toolbox_2.6/met')
%addpath('~/apps/tensorlab')
