function wxstar = histwx(W,llhfunc,dpgm,offset,tgradius,type)

numbin = 200;

if type == 1 % batch mode
    
    logpx = llhfunc(W) + offset;
    logfx = logdpgmpdf(W,dpgm,tgradius);
    ix = logfx ~= -Inf;
    logwx = logpx(ix) - logfx(ix);
    
elseif type == 2 % compute isinregion one by one
  
    logfx = logdpgmpdf(W,dpgm,tgradius);
    ix = logfx ~= -Inf;
    logpx = zeros(sum(ix),1);
    j = 0;
    for i = 1:length(ix)
            if ix(i) == 1
                j = j + 1;
                logpx(j) = llhfunc(W(i,:)) + offset; 
            end
    end
    assert(length(logpx) == length(logfx(ix)))
    logwx = logpx - logfx(ix);
    
else
    error('undefined type');
end

[bin,xout] = hist(logwx,numbin);

if 1
    [~,imax] = max(bin);
    wxstar = xout(imax);
else
    wxstar = xout*bin'/sum(bin);
end

