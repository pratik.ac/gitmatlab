function run_HMC_P_mog(seed,dim,iterburnin,...
                                     mxit,itvplot,itvprint,itvsave,PLOT,...
                                     mog,hmc,nparallel,maxruntime,itersync)

% clear
% close all
dbstop if error
addpath('../00_utils');

if isdeployed
        
        seed = str2double(seed);
        dim = str2double(dim);
        iterburnin = str2double(iterburnin); 
        mxit = str2double(mxit); 
        hmc.lfn = str2double(lfn);
        hmc.lfsize = str2double(lfsize);
        itvplot = str2double(itvplot);
        itvsave = str2double(itvsave);
        itvprint = str2double(itvprint);
        PLOT = str2double(PLOT);
        nparallel = str2double(nparallel);
        maxruntime = str2double(maxruntime);
        itersync = str2double(itersync);
        
end

dock = 0;

%% random stream
RandStream.setDefaultStream(RandStream('mt19937ar','Seed',seed));

%%
gt.mu = mog.pik*mog.mu';


%% Init Parallelization
sz.pool = matlabpool('size');

if sz.pool > 0 && sz.pool ~= nparallel && nparallel ~= 0
        
        matlabpool close
        matlabpool(nparallel);        
        
end

if sz.pool == 0 && nparallel ~= 0
        
        matlabpool(nparallel);
        
end

maxNumCompThreads(2);

%% get function handles
[llhfunc,gradfunc] = funcslogmog(mog);
% clear ld

%% fname
fname = sprintf('HMC_mog_para_sd%d_d%d_k%d_para%d_nrestart%d_rt%d', seed, dim, mog.k, nparallel,maxruntime);
fname = strrep(fname,'.','-');
fname = appendfilehead(fname);
fname = strcat('_',fname);

%% plots
nhist = 4;
for j = 1:nhist
        fig.hist3(j) = figure(440+j); clf;
end

fig.rerror_mu = figure(460); clf;
% fig.rerror_cov = figure(451); clf;
fig.R = figure(462); clf;
% fig.locerr = figure(453); clf;

if dock && PLOT && ~isdeployed
        
        for j = 1:nhist
                set(fig.hist3(j),'windowstyle','docked');
        end
        set(fig.rerror_mu,'windowstyle','docked');   
%         set(fig.rerror_cov,'windowstyle','docked');   
        set(fig.R,'windowstyle','docked');   
%         set(fig.locerr,'windowstyle','docked');
        
end

pause(0.5);
figrow = 1;
% nfig = figrow^2;
nfig = 1;
% nfig = 1;
nfig = min(nfig,dim-1);
% cvpair = [1:nfig; mod((1:nfig) + dim/2 -1, dim)+1]';
cvpair = [1 2; 3 4; 5 6; 7 8];
colors = distinguishable_colors(mog.k);

%% draw components
for qq = 1:nhist
        
        set(0,'CurrentFigure',fig.hist3(qq)); 
        linewidth = 1;

        for f=1:nfig

                subplot(figrow,figrow,f);        
                hold on;
                ii = cvpair(f,1);
                jj = cvpair(f,2);

                for kk=1:mog.k

                        plotGauss(mog.mu(ii,kk),mog.mu(jj,kk),...
                                         mog.cv(ii,ii,kk),mog.cv(jj,jj,kk),mog.cv(ii,jj,kk),...
                                         2,'--k',linewidth); hold on;  
%                         plot(mog.mu(ii,kk),mog.mu(jj,kk),'color',colors(kk,:),'marker','x','markersize',10,'linewidth',2);                        
                        plot(mog.mu(ii,kk),mog.mu(jj,kk),'k+','markersize',10,'linewidth',2);            


                end
                axis tight
                axax(f,:) = axis*1.2;

        end
        
end

drawnow;

%% draw mog hist3(qq)
ax = max(abs(axax),[],1);
ax([1 3]) = -1*ax([1 3]);
rangx = linspace(ax(1),ax(2),100);
rangy = linspace(ax(3),ax(4),100);
[xx yy] = meshgrid(rangx,rangy);
xv = xx(:);
yv = yy(:);
xy = [xv yv];
prxy = zeros([size(xx)  mog.k]);

for i=1:nfig

        subplot(figrow,figrow,i); hold on;          
        ii = cvpair(i,1);
        jj = cvpair(i,2);
        density = exp(logmogpdf(xy,mog.mu([ii jj],:),mog.cv([ii,jj],[ii,jj],:),mog.pik));              
        prxy(:,:,i) = reshape(density,length(rangy),length(rangx));    
        contour(xx, yy,prxy(:,:,i),15); colormap default
        
end

drawnow;

SAMP = cell(nparallel,1);
for qq = 1 : nparallel
        SAMP{qq} = zeros(dim, mxit*2);        
end
TIME = zeros(mxit,1);
R = zeros(ceil(mxit/itvplot)+100,1);
REMU = zeros(ceil(mxit/itvplot)+100,1);
REMUT = zeros(ceil(mxit/itvplot)+100,1);

nhmcaccept = zeros(nparallel,1);
hmcaccept = zeros(nparallel,1);
nhmc = 0;
tottime = 0;

%% overdispersed init
rangemin = min(mog.mu')*2;
rangemax = max(mog.mu')*2;     
wcurr = repmat(rangemin',1,nparallel) + rand(dim,nparallel).*repmat((rangemax'-rangemin'),1,nparallel);

last_t_plot = 0;
s = zeros(nparallel,1);
t = 0;
p = 0;
v = 0;

%% mode search
while t < mxit && tottime < maxruntime
        
        t = t + itersync;
        
        tic
        
        parfor qq = 1:nparallel
                
                u = 0;
                
                while u < itersync
                        
                        u = u + 1;
                        
                        if ~rem(u+50,50)
                                if t < iterburnin
                                        sprintf('qq:%d, u:%d (burnin)',qq,u)
                                else
                                        sprintf('qq:%d, u:%d',qq,u)
                                end
                        end

                        % 1st KERNEL, HMC            
                        [wcurr(:,qq),hmcaccept(qq)] = hmcfunc(wcurr(:,qq), llhfunc, gradfunc, hmc);
                        % avgprhmcaccept(qq) = (1-1/t) * avgprhmcaccept(qq)+ 1/t * prhmcaccept(qq);
        
                        nhmcaccept(qq) = nhmcaccept(qq) + hmcaccept(qq);

                        if t > iterburnin
                                s(qq) = s(qq) + 1;
                                SAMP{qq}(:,s(qq)) = wcurr(:,qq);
                        end

                end
                
        end
        
        tottime = tottime + toc;
        
        
        
        %% save
        if (floor(last_t_plot/itvsave) ~= floor(t / itvsave)) && p > 0
                
                v = v + 1;
                
                cd 00_log_working
                save(fname);
                
                disp('---------------- SAVE COMPLETE! ----------------');           
                cd ..                
                
        end
        
        
        
        
        %% plot
        if (floor(last_t_plot/itvplot) ~= floor(t / itvplot)) && all(s > 0)
                
                tic
                
                p = p + 1;
                        
                t
                hmcacceptrate = nhmcaccept./t
                
                
                %% R convergence Diagnostic                      
                SAMP_FOR_R = zeros(min(s), dim, nparallel);
                for qq = 1 : nparallel
                        SAMP_FOR_R(:,:,qq) = SAMP{qq}(:,1:s(qq))';
                end
                R(p) = mpsrf(SAMP_FOR_R);
                
                %% compute error
                combsamp = [];
                for qq=1:nparallel                        

                        sampmu = mean(SAMP{qq}(:,1:s(qq))');
                        REMU(p,qq) = sum(abs(gt.mu - sampmu))/sum(abs(gt.mu));
                        combsamp = [combsamp SAMP{qq}(:,1:s(qq))];
                        
                end                
                combsampmu = mean(combsamp,2);
                REMUT(p) = sum(abs(gt.mu - combsampmu'))/sum(abs(gt.mu));
                
                TIME(p) = tottime + toc;

                
                %%
                set(0, 'currentfigure', fig.R); clf        
                plot(TIME(1:p), R(1:p), '-+k'); hold on; 

                set(0, 'currentfigure', fig.rerror_mu); clf;                                
                if p > 1 && nparallel > 1
                        % plot(cumtime(itv), mean(REMU(1:p,:)'), '-r'); hold on;
                        shadedErrorBar(TIME(1:p),mean(REMU(1:p,:)'), std(REMU(1:p,:)'),...
                                {'r-+','markerfacecolor','r'},1); hold on;
                        plot(TIME(1:p),REMUT(1:p),'-+b');
                else
                        plot(TIME(1:p), mean(REMU(1:p,:)'),'-r'); hold on;
                end
                
                drawnow;
                               
                %% plot
                        
                for qq=1:nhist

                        set(0,'CurrentFigure',fig.hist3(qq)); clf;

                        for ff = 1:nfig

                                subplot(figrow,figrow,ff);             
                                ii = cvpair(ff,1);
                                jj = cvpair(ff,2);   

                                contour(xx, yy,prxy(:,:,ff),10); hold on;

                                for kk = 1:mog.k

                                        plot(mog.mu(ii,kk),mog.mu(jj,kk),'k+','markersize',10,'linewidth',2);            

                                end
                                                                
                                plot(SAMP{qq}(ii,1:s(qq)),SAMP{qq}(jj,1:s(qq)),'b.','markersize',1);
                                plot(wcurr(ii,qq),wcurr(jj,qq),'b.','markersize',10,'linewidth',2);                
                                
                        end

                        hold off
                        axis(axax(ff,:));
                
                        title(strrep(fname,'_','-'));
                        drawnow;
                        
                end               
                
        end

        last_t_plot = t;
        
        
end                 


%% save when finished
fname = fname(2:end);
cd 00_log_finished

% saveas(fig.hist3(1),strcat(fname,'_HIST1.png'),'png');
% saveas(fig.hist3(2),strcat(fname,'_HIST2.png'),'png');
% saveas(fig.R,strcat(fname,'_R.png'),'png');
% saveas(fig.rerror_mu,strcat(fname,'_REMU.png'),'png');
% saveas(fig.rerror_cov,strcat(fname,'_RECV.png'),'png');
% 
% saveas(fig.hist3(1),strcat(fname,'_HIST1.fig'),'fig');
% saveas(fig.hist3(2),strcat(fname,'_HIST2.fig'),'fig');
% saveas(fig.R,strcat(fname,'_R.fig'),'fig');
% saveas(fig.rerror_mu,strcat(fname,'_REMU.fig'),'fig');
% saveas(fig.rerror_cov,strcat(fname,'_RECV.fig'),'fig');
for qq = 1:nparallel
        SAMP{qq} = SAMP{qq}(:,1:s(qq)); 
end
TIME = TIME(1:p);
REMU = REMU(1:p,:);
REMUT = REMUT(1:p);
R = R(1:p);
save(fname);
cd ..
disp('---------------- RUN COMPLETE! ----------------');           
% save(fname,'SAMP','s','seed','dim','lfnum','lfsize','nrestart','ninitsamp','iterburnin','nsubsamp');
                 