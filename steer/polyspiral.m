%function polyspiral(k0, xf, yf, thf, kf)

clc; clear all;

k0 = 0.1;
kf = 0.1;
xf = 1;
yf = 1;
thf = 0.01;

tol = 0.02;

% vars = q = (b, c, d, sf)
b = 1;
c = 1;

sf = norm([xf yf]);
d = sf;

q = [b c d sf]';
eta = 1;
iter = 0;
eps = 1;
while true,
	b = q(1); c = q(2); d = q(3); sf = q(4);

	if sf < 0 || iter > 100 || eps < tol,
		break;
	end

	bc = [sf, sf^2; sf^2/2, sf^3/3]\[kf-k0-d*sf^3; thf-k0*sf-d*sf^4/4];
	b= bc(1);
	c = bc(2);
	s = 0:0.05:sf;

	%cdv = [sf^2 sf^3; sf^3/3 sf^4/4]\[kf-k0-b*sf; thf-k0*sf-b*sf^2/2];
	%c = cdv(1);
	%d = cdv(2);

	% Newton's method
	Pk = [d c b k0];
	Pth = polyint(Pk);

	S = @(k)(quadl(@(s)(s.^k .* cos(polyval(Pth, s))), 0, sf));
	C = @(k)(quadl(@(s)(s.^k .* sin(polyval(Pth, s))), 0, sf));

	A = [-S(2)/2 -S(3)/3 -S(4)/4 cos(thf);
		  C(2)/2 C(3)/3 C(4)/4 sin(thf)]
	g = [quadl(@(s)(cos(polyval(Pth, s))), 0, sf) - xf;
		 quadl(@(s)(sin(polyval(Pth, s))), 0, sf) - yf]

	if norm(g) < tol,
		break;
	end

	dq = -eta*A\g
	qnew = [b c d sf]' + dq;
	
	eps = min(norm(g), norm(dq));
	fprintf('eps: %f\n', eps);

	q = qnew;
	%ans = input('');
	iter = iter + 1;
end

if sf < 0,
	fprintf('sf is %f\n', sf);
end

d = q(1);
sf = q(2);
qfull = [k0 b c d sf]'
s = 0:0.05:sf;

Pk = [d c b k0];
Pth = polyint(Pk);
xfunc = cos(polyval(Pth, s));
yfunc = sin(polyval(Pth, s));

kvec = polyval(Pk, s);
thvec = polyval(Pth, s);
xvec = cumtrapz(s, xfunc);
yvec = cumtrapz(s, yfunc);

figure(1); clf;
plot(s, kvec, 'b-', 'Linewidth', 1.5);
hold on;
plot(s, thvec, 'r--', 'Linewidth', 1.5);

figure(2); clf;
plot(s, xvec, 'b-', 'Linewidth', 1.2);
hold on;
plot(s, yvec, 'r-', 'Linewidth', 1.2);
axis equal;

%end
