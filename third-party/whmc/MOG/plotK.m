clear
close all
% addpath('drawdata');
addpath('../00_utils');
dbstop if error


K = [5 10 15 20];
nK= 4;
Algo = {'drmc','lmcwh'};
nAlgo = 2;
colors = distinguishable_colors(nAlgo);

files = dir('./dataK/');
nfiles = length(files) - 2;

mc = cell(1,nAlgo*nK);
for kk=1:nK
    for Alg=1:nAlgo
        for i=1:nfiles
            if ~isempty(strfind(files(i+2).name,Algo{Alg}))&~isempty(strfind(files(i+2).name,strcat('k',num2str(K(kk)))))
                mc{(kk-1)*nAlgo+Alg} = load(strcat('./dataK/', files(i+2).name));
            end
        end
    end
end


%% K-R plot
cuttime = 400;
cuttimeR = zeros(nK*nAlgo,1);
cuttimeREM = zeros(nK*nAlgo,1);

for kk = 1:nK
    for i = 1:nAlgo
            ii = (kk-1)*nAlgo + i;
            ixt = sum(mc{ii}.TIME(1:mc{ii}.p) < cuttime);
            if ixt > mc{ii}.p
                   ixt = mc{ii}.p;
            end
            cuttimeR(ii) = mc{ii}.R(ixt); 
            cuttimeREM(ii) = mc{ii}.REMU(ixt);
    end
end

styles = {'-o','--+','-s','--x'};

% fig.KR = figure(103); clf;
% set(fig.KR,'windowstyle','docked');
% for i = 1:nAlgo
%     plot(K(1:end),cuttimeR(i:nAlgo:end)',styles{i},'color',colors(i,:),'linewidth',2,'markersize',10); hold on;
% end
% set(gca,'FontSize',15);
% xlabel('Number of Components','FontSize',18); ylabel('R at time = 400 sec ','FontSize',18);
% legend('RDMC','WORMHOLE','FontSize',18,'location','NorthWest');
% title('R vs Num of Components (D=20)','FontSize',20);


fig.KREM = figure(241); clf;
set(fig.KREM,'windowstyle','docked');
for i = 1:nAlgo
    plot(K(1:end),cuttimeREM(i:nAlgo:end)',styles{i},'color',colors(i,:),'linewidth',2,'markersize',10); hold on;
end
ylim([0,1]);
set(gca,'FontSize',15);
xlabel('K','FontSize',18); ylabel('REM (after 400 sec)','FontSize',18); 
legend('RDMC','WHMC','FontSize',18,'location','NorthWest');
title('D=20','FontSize',20);

