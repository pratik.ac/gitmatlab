%REJECPROP =  0, method 1
%             1, method 2
%             4, method 3 uniform distribution
%             5, method 4 uniform, initialized by mode search + Laplace

dim = 8; la = .01;
REJECPROP = 4;
seed = sum(100*clock);
m = 1;
itvplot = 2000; itvsave = 30000; itvprint = 500;

lfnum = 10;
lfsize = 1.5e-2; %0.001; %3e-5;

nrestart = 10;
iterburnin = nrestart * 1000;
ninitsamp = nrestart * 500;
nsubsamp = 10000; % 5000
mxit = 220000;

troff = 1.9; trquant = .5; maxncompo = 100;

samp4adapt = 1000;
% time4adapt = 20; %50;
PLOT = 1;

%%%%%%%%%%%%%%%%%%%%%%%%
% wormhole
TrajectoryLength = .14;
NumOfLeapFrogSteps = 10;
StepSize = TrajectoryLength/NumOfLeapFrogSteps;
NewtonSteps = 4;

run_drmcmc_wsn_serial(seed,dim,troff,...
                                     REJECPROP,m,maxncompo,...
                                     ninitsamp,iterburnin,nrestart,samp4adapt,nsubsamp,...
                                     mxit,lfnum,lfsize,itvplot,itvprint,itvsave,PLOT);

% run_wh_wsn(seed,dim,ninitsamp,iterburnin,nrestart,mxit,...
%                 TrajectoryLength,NumOfLeapFrogSteps,NewtonSteps,itvplot,itvprint,itvsave,PLOT);
                     
                                   