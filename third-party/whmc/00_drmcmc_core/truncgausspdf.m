function tgpdf = truncgausspdf(X,MU,SIGMA,tgradius)

tgpdf = exp(logtruncgausspdf(X,MU,SIGMA,tgradius));
