% clear
% close all
% addpath('drawdata');
addpath('../00_utils');
dbstop if error

% time plot for D=20,K=10

Algo = {'drmc','lmcwh'};
nAlgo = 2;
colors = distinguishable_colors(nAlgo);
styles = {'--o','-.+','-s','--x'};


files = dir('./dataD');
nfiles = length(files) - 2;

for Alg=1:nAlgo
    for i=1:nfiles
        if ~isempty(strfind(files(i+2).name,Algo{Alg}))&~isempty(strfind(files(i+2).name,'d20_k10'))
            mc{Alg} = load(strcat('./dataD/', files(i+2).name));
        end
    end
end


% set common time points
cut1 = sum(mc{1}.TIME<800);
for i=1:cut1
    ixt = sum(mc{2}.TIME(1:mc{2}.p) < mc{1}.TIME(i));
    cuttime(i) = ixt;
end


fig.REM = figure(200); clf;
set(fig.REM,'windowstyle','docked');

%% REM
plot(mc{1}.TIME(1:cut1), mc{1}.REMU(1:cut1), styles{1},'color',colors(1,:),'linewidth',2,'markersize',10); hold on;
plot(mc{2}.TIME(cuttime),mc{2}.REMU(cuttime),styles{2},'color',colors(2,:),'linewidth',2,'markersize',10); drawnow;
ylim([0,1]);
set(gca,'FontSize',15);
xlabel('Seconds','FontSize',18); ylabel('REM','FontSize',18); 
legend('RDMC','WORMHOLE','FontSize',18,'location','NorthEast');
title('REM (D=20, K=10)','FontSize',20);    


fig.R = figure(204); clf;
set(fig.R,'windowstyle','docked');

%% R
plot(mc{1}.TIME(1:cut1), mc{1}.R(1:cut1), styles{1},'color',colors(1,:),'linewidth',2,'markersize',10); hold on;
plot(mc{2}.TIME(cuttime),mc{2}.R(cuttime),styles{2},'color',colors(2,:),'linewidth',2,'markersize',10); drawnow;
ylim([1,2.5]);
set(gca,'FontSize',15);
xlabel('Seconds','FontSize',18); ylabel('R','FontSize',18); 
legend('RDMC','WORMHOLE','FontSize',18,'location','NorthEast');
title('R (D=20, K=10)','FontSize',20); 

