function [tf dist]= isinregion(x,mu,cv,radius)

[~, k] = size(mu);
tf = zeros(k,1);
dist = zeros(k,1);

for kk=1:k
    invU = inv(chol(sqrtm(cv(:,:,kk))));
    xx = mu(:,kk) + (invU*invU')*(x'-mu(:,kk));
    dist(kk) = norm(xx-mu(:,kk));
    tf(kk) = dist(kk) < radius;
end


