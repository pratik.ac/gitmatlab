function [tf, dist]= isinregionL(x,mu,L,radius)
% x: Dim x N
% tf: K x N
% dist: K x N

[~, k] = size(mu);
n = size(x, 2);
dist = zeros(k,n);

for kk=1:k
    xx = L(:,:,kk)\bsxfun(@minus, x, mu(:,kk));
    dist(kk, :) = sqrt(sum(xx.^2, 1));
end
tf = dist < radius;

% figure(1)
% [~,k] = min(dist);
% plot(1:length(x), mu(:, k), 1:length(x), x); title([dist(k) radius]);
% pause
