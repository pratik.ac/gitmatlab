% clear
% close all
% addpath('drawdata');
addpath('../00_utils');
dbstop if error

% time plot for D=20,K=10

Algo = {'drmc','wormhole'};
nAlgo = 2;
colors = distinguishable_colors(nAlgo);
styles = {'-','--','-.','-s','--x'};


files = dir('./result');
nfiles = length(files) - 2;

for Alg=1:nAlgo
    for i=1:nfiles
        if ~isempty(strfind(files(i+2).name,Algo{Alg}))
            mc{Alg} = load(strcat('./result/', files(i+2).name));
        end
    end
end


% set common time points
for i=1:nAlgo
    ixt = sum(mc{i}.TIME&mc{i}.TIME < 800);
    cuttime(i) = ixt;
end
% cuttime =[floor(1:cuttime(1)/4:cuttime(1)) cuttime];

fig.REM = figure(200); clf;
% set(fig.REM,'windowstyle','docked');

%% REM
for i=1:nAlgo
    plot(mc{i}.TIME(1:cuttime(i)), mc{i}.REMU(1:cuttime(i)), styles{i},'color',colors(i,:),'linewidth',2); hold on;
end
drawnow;
ylim([0,.17]);
set(gca,'FontSize',15);
xlabel('Seconds','FontSize',18); ylabel('REM','FontSize',18); 
legend('RDMC','WHMC','FontSize',18,'location','NorthWest');
title('RDMC vs WHMC','FontSize',20);    


% fig.R = figure(204); clf;
% % set(fig.R,'windowstyle','docked');
% 
% %% R
% for i=1:nAlgo
%     plot(mc{i}.TIME(1:cuttime(i)), mc{i}.RR(1:cuttime(i)), styles{i},'color',colors(i,:),'linewidth',2); hold on;
% end
% set(gca,'FontSize',15);
% xlabel('Seconds','FontSize',18); ylabel('REM','FontSize',18); 
% legend('RDMC','WORMHOLE','FontSize',18,'location','best');
% title('R','FontSize',20); 

