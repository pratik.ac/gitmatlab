function dpgm = dpgaussmixture_Yutian(X,N,K,algo,uniform,dpgm)
% X: D x Nmax
% N: number of samples for training

if ~exist('uniform', 'var') || isempty(uniform)
  uniform = false; % Do uniform sampling for darting
end

if ~exist('dpgm', 'var') || isempty(dpgm)
  update = false; % New model
else
  update = true;
end


% !!!!!!!!!!!!!!!!!!!!!!!!!
% update = false;


% Prepare data
Nmax = size(X, 2);
N = min(N, Nmax);

% Training data index
ixsubsamp = ceil((1:N)/N * Nmax);
if update
  % #samples from old data
  Nretain = find(ixsubsamp <= dpgm.ixsub(end), 1, 'last');
  assert(Nretain <= length(dpgm.ixsub));
  
  % subsampling
  ix_in_dpgm = ceil((1:Nretain)/Nretain * length(dpgm.ixsub));
  ixsubsamp(1 : Nretain) = dpgm.ixsub(ix_in_dpgm);
  
  % initialize q_of_z
  dpgm.q_of_z(1 : Nretain, :) = dpgm.q_of_z(ix_in_dpgm, :);
  dpgm.q_of_z(dpgm.q_of_z < 1e-10) = 0;
  dpgm.q_of_z(Nretain+1:N,:) = rand(N - Nretain, dpgm.k);
  dpgm.q_of_z = dpgm.q_of_z ./ repmat(sum(dpgm.q_of_z, 2), 1, dpgm.k);
  
  if isequal(algo, 1) % vdp
    dpgm.q_of_z = [dpgm.q_of_z, zeros(N, 1)];
  end
end
dpgm.ixsub = ixsubsamp;

% training data
X = X(:,ixsubsamp);

[dim,N] = size(X);
switch algo
    case 1
        opts = mkopts_avdp();
    case 2
        opts = mkopts_csb(K);
    case 3
        opts = mkopts_cdp(K);
    case 4
        opts = mkopts_bj(K);
    otherwise
        error('undefined algorithm type');
end
opts.get_q_of_z = 1;

if update
  opts.use_kd_tree = 0;
  opts.q_of_z = dpgm.q_of_z;
end

result = vdpgm(X,opts);
nk = result.K;
mu = result.hp_posterior.m;
qz = result.q_of_z;
Nk = sum(qz);

%% 
if ~all(Nk)
    nk = nk - sum(Nk==0);
    mu(:,Nk==0) = [];
    qz(:,Nk==0) = [];
    Nk(:,Nk==0) = [];    
end


%%
cv = zeros(dim,dim,nk);
L = zeros(dim,dim,nk);
for k=1:nk
    muk =  mu(:,k);
    Y = repmat(sqrt(qz(:,k)),1,dim)'.*(X - repmat(muk,1,N));
    cvk = Y*Y'/Nk(k) + eye(dim)*0.00001;
    cv(:,:,k) = (cvk+cvk')/2;
    L(:,:,k) = chol(cv(:,:,k), 'lower');
end


if ~uniform % Gaussian mixture
  
  pik =  Nk/N;
  pik = pik/sum(pik);
  
else % pik \propto volume
  
  pik = zeros(1, nk);
  for k = 1 : nk
    pik(k) = sum(log(diag(L(:,:,k))));    
  end
  pik = exp(pik - logsumexp(pik));
  pik = pik / sum(pik);
  
end
  

%% choose top 'K' components
if algo == 1 && K < nk
    nk = K;
    mu = mu(:,1:nk);
    cv = cv(:,:,1:nk);
    L = L(:,:,1:nk);
    pik = pik(1:nk)/sum(pik(1:nk));
end

dpgm.mu = mu;
dpgm.cv = cv;
dpgm.L = L;
dpgm.pik = pik;
dpgm.k = nk;
dpgm.result = result;
dpgm.uniform = uniform;
dpgm.q_of_z = qz;