function [G InvG dG Gamma1] = metric(x,opt)
    if(nargin<2)
        opt=0;
    end
    
    dim = size(x,1);
    M_base = eye(dim);
    G = M_base;

    if all(opt==0)
        InvG=NaN; dG=NaN; Gamma1=NaN;
    else
        if any(opt==-1)
            InvG = inv(G);
        end
        if any(opt==1)
            dM_base = zeros(repmat(dim,1,3));
            dG = zeros(repmat(dim,1,3));
            if any(opt==3)
                Gamma1 = .5*(permute(dG,[1,3,2]) + permute(dG,[3,2,1]) - dG);
            else
                Gamma1=NaN;
            end
        else
            dG=NaN; Gamma1=NaN;
        end
    end
    
end