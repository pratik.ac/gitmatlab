function y = isrow(x)

[a,b] = size(x);

y = (a==1 && b>1);