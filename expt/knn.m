clc;
n = 1000;
d = 100;
k = 10;

X = rand(n,d);
Y = (-1) .^ ((rand(n,1) - 0.5) > 0);

Dc = pdist2(X,X);
D = Dc;
[tmp1, ind] = sort(D, 'descend');
tmp2 = sub2ind(size(D), ind(k+1:end,:), repmat(1:size(D,2), size(D,1)-k, 1));
D(tmp2) = 0;
D = D > 0;
D = D + eye(n);
Yh = D*Y ./ sum(D,2);